#ifndef PARTACA_H_
#define PARTACA_H_

#include "harith.h"

typedef field (*entry)(uint i, uint j, uint k, uint l, uint m, uint n);

void partaca(uint rows, uint cols, uint *ridx, uint *cidx, real accur, prkmatrix R, uint k, uint l, uint m, uint n, entry func);

void fill_entry(uint rows, uint cols, uint *ridx, uint *cidx, field *drct, uint k, uint l, uint m, uint n, entry func);

#endif
