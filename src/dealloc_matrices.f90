subroutine dealloc_bem_matrices
  use globals
  implicit none

  select case(islv)
  case(-1)
     deallocate(smat,dmat,amat)
  case(0)
     deallocate(smat,dmat,subc,cbinv,subb,ipiv_b)
  case(1)
     deallocate(smat,dmat,subc,cbinv,hbmat)
  case(2)
     deallocate(hbmat,hcmat,hcbinvmat,hdmat_i,hdmat_d,hemat_i,hemat_d)
  case(3)
     deallocate(hbmat,hcmat,hdmat_i,hdmat_d,hemat_i,hemat_d)
  end select

end subroutine dealloc_bem_matrices

