subroutine hermitian_conjugate(rows,cols,cmat,cmat_herm)
  implicit none
  integer::j
  integer,intent(in)::rows,cols
  complex(8),intent(in)::cmat(rows,cols)
  complex(8),intent(out)::cmat_herm(cols,rows)

  do j=1,cols
     cmat_herm(j,:)=conjg(cmat(:,j))
  end do

end subroutine hermitian_conjugate
