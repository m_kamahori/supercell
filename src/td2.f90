subroutine topological_derivative2
  use globals
  use io
  implicit none
  integer::i,j,k,ibril,ix,iy,itmp,jtmp
  integer::iltc,jltc
  real(8)::dx,dy,rglrz,ltcl,tmp(nip),td_ev(nip),sens(nip),reg
  real(8),parameter::gosa=1.d-6
  character(len=32)::stepname,nx
  type(lattice)::ltc(2,2)

  
  sens(:)=0.d0
  do ibril=1,nbril
     tmp(:)=0.d0
     td_ev(:)=0.d0
     do k=1,ev(ibril)%n
        !calculate T_{ij}
        call td_for_each_ev(nip,ev(ibril)%uin(:,k),ev(ibril)%duin(:,:,k),ev(ibril)%s(k),&
             td_ev(:))
        if(td_ev(1).ne.td_ev(1))stop "td_ev(1)=NaN"
        if(real(ev(ibril)%s(k)) < t_omg)td_ev(:)=-td_ev(:)
        tmp(:)=tmp(:)+td_ev(:)
     end do !k
     sens(:)=sens(:)+tmp(:)/ev(ibril)%n
  end do
  sens(:)=sens(:)/(nbril*t_omg**2)

  !トポロジー導関数が計算されたところ(naiten)をtd(:,:)に放り込む
  allocate(td(0:xe,0:xe))
  td(:,:)=0.d0
  do iy=0,ye
     do ix=0,xe
        ! dx=xelm*dble(ix)
        ! dy=yelm*dble(iy)
        dx=dble(ix)/dble(xe)*uvec(1,1)+dble(iy)/dble(xe)*uvec(1,2)
        dy=dble(ix)/dble(xe)*uvec(2,1)+dble(iy)/dble(xe)*uvec(2,2)

        do i=1,nip
           ! write(500,*) dx,dy,td(ix,iy)
           if(dx-gosa<grid(1,i) .and. grid(1,i)<dx+gosa .and. dy-gosa<grid(2,i) .and. grid(2,i)<dy+gosa)then !naiten
              td(ix,iy)=-sens(i) !minimisation
           end if
        end do
        !        if(lsfunc(ix,iy).lt.0.d0) td(ix,iy)=-td(ix,iy) !minimisation
        ! if(lsfunc(ix,iy).ge.0.d0) td(ix,iy)=-td(ix,iy) !maximisation
        ! evol=evol+abs(td(ix,iy))

     end do
  end do

  deallocate(grid)

  ! do iy=0,ye
  !    do ix=0,xe
  !       dx=dble(ix)/dble(xe)*uvec(1,1)+dble(iy)/dble(xe)*uvec(1,2)
  !       dy=dble(ix)/dble(xe)*uvec(2,1)+dble(iy)/dble(xe)*uvec(2,2)
  !       write(900,*) dx,dy,td(ix,iy)
  !    end do
  !    write(900,*)
  ! end do

  call impose_symmetry_avg(xe,td(:,:))

  ! do iy=0,ye
  !    do ix=0,xe
  !       dx=dble(ix)/dble(xe)*uvec(1,1)+dble(iy)/dble(xe)*uvec(1,2)
  !       dy=dble(ix)/dble(xe)*uvec(2,1)+dble(iy)/dble(xe)*uvec(2,2)
  !       write(909,*) dx,dy,td(ix,iy)
  !    end do
  !    write(909,*)
  ! end do

  ! トポロジー導関数を正規化
  reg=maxval(abs(td(:,:)))
  write(*,*) reg
  reg=1/reg
  td=td*reg

  ! 最も外側の格子内に要素が出来ないようにする
  do iy=1,ye-1
     if(iy.eq.1.or.iy.eq.ye-1)then
        do ix=1,xe-1
           td(ix,iy)=0.d0
        end do
     else
        td(1,iy)=0.d0;td(xe-1,iy)=0.d0
     end if
  end do

  ! do iy=0,ye
  !    do ix=0,xe
  !       dx=dble(ix)/dble(xe)*uvec(1,1)+dble(iy)/dble(xe)*uvec(1,2)
  !       dy=dble(ix)/dble(xe)*uvec(2,1)+dble(iy)/dble(xe)*uvec(2,2)
  !       write(903,*) dx,dy,lsfunc(ix,iy)
  !    end do
  ! end do
  
  
  ! 境界近傍の格子点におけるトポロジー導関数をゼロにする
  ltcl=xsize/dble(xe)
  ! ltcl=l1/dble(xe)
  do iy=1,ye-1
     do ix=1,xe-1
        ! 格子点(ix,iy)を含む格子情報作成
        do j=iy,iy+1
           do i=ix,ix+1
              iltc=i-ix+1
              jltc=j-iy+1
              call lattice_info(lsfunc(0:xe,0:ye),ltcl,ix,iy,i,j,ltc(iltc,jltc))
              if(ltc(iltc,jltc)%flag.eq.1)then
                 write(*,*) "td on ",ix,iy,"equals to zero"
                 td(ix,iy)=0.d0
              end if
           end do
        end do
     end do
  end do

  open(500,file=file_td)
  do iy=0,ye
     do ix=0,xe
        dx=dble(ix)/dble(xe)*uvec(1,1)+dble(iy)/dble(xe)*uvec(1,2)
        dy=dble(ix)/dble(xe)*uvec(2,1)+dble(iy)/dble(xe)*uvec(2,2)
        write(500,*) dx,dy,td(ix,iy)
     end do
     write(500,*)
  end do
  close(500)

  deallocate(ugrid,dugrid)
contains
  subroutine td_for_each_ev(nip,uin,qin,evij,t)
    integer               ::i
    integer   ,intent(in) ::nip
    complex(8),intent(in) ::evij
    complex(8),intent(in) ::uin(nip),qin(2,nip)
    real(8)   ,intent(out)::t(nip)
    real(8)::rglrz

    rglrz=0.d0

    do i=1,nip
       rglrz=rglrz+(real(uin(i))**2+aimag(uin(i))**2)*menseki/dble(nip)
       ! rglrz=rglrz+(real(uin(i))**2+aimag(uin(i))**2)*ds
    end do
    ! rglrz=1.d0/sqrt(rglrz)        !regularizer←結構アバウト?

    do i=1,nip              !topological derivative calculation
       t(i)=-2.d0*(real(qin(1,i))**2+aimag(qin(1,i))**2&
            +real(qin(2,i))**2+aimag(qin(2,i))**2)&
            +real(evij)**2*(real(uin(i))**2+aimag(uin(i))**2)
       ! t(i)=-2.d0*((rglrz*real(qin(1,i)))**2+(rglrz*aimag(qin(1,i)))**2&
       !      +(rglrz*real(qin(2,i)))**2+(rglrz*aimag(qin(2,i)))**2)&
       !      +real(evij)**2*((rglrz*real(uin(i)))**2+(rglrz*aimag(uin(i)))**2)
    end do
    t(:)=t(:)/rglrz

  end subroutine td_for_each_ev

end subroutine topological_derivative2

