subroutine error_mat(rows,cols,amat,bmat)
  implicit none
  integer::i,j
  integer,intent(in)::rows,cols
  real(8)::error,tmp
  complex(8),intent(in)::amat(rows,cols),bmat(rows,cols)

  error=0.d0
  do i=1,rows
     do j=1,cols
        tmp=abs(amat(i,j)-bmat(i,j))/abs(bmat(i,j))
        ! if(tmp.gt.1.d-3) write(*,*) i,j,tmp
        error=error+tmp
        ! write(10,*) subc(i,j,iskbn)
        ! write(10,*) workmat(i,j,iskbn)
     end do
     ! write(10,*)
  end do
  error=error*1.d2
  write(*,*) "error=",error,"%"

  ! if(error.gt.1.0d2)then
  !    call write_mat(rows,cols,amat,1111)
  !    call write_mat(rows,cols,bmat,1112)
  !    stop "error_mat"
  ! end if

end subroutine error_mat

subroutine write_cmat(rows,cols,mat,num)
  implicit none
  integer::i,j,num
  integer,intent(in)::rows,cols
  complex(8),intent(in)::mat(rows,cols)

  do i=1,rows
     do j=1,cols
        write(num,*) i,j,mat(i,j)
     end do
  end do

end subroutine write_cmat

subroutine write_rmat(rows,cols,mat,num)
  implicit none
  integer::i,j,num
  integer,intent(in)::rows,cols
  real(8),intent(in)::mat(rows,cols)

  do i=1,rows
     do j=1,cols
        write(num,*) i,j,mat(i,j)
     end do
  end do

end subroutine write_rmat

subroutine checktime(st,intvl)
  use omp_lib
  implicit none
  real(8),intent(inout)::st
  real(8),intent(out)::intvl
  real(8)::en

  en=omp_get_wtime()
  intvl=en-st

  if(st.ne.0d0)write(*,'(A7,f21.14,A2)') "intvl:",intvl,"s"

  st=en

end subroutine checktime

subroutine iswap(n,a,b)
  implicit none
  integer::i,n
  integer,intent(inout)::a(n),b(n)
  integer::tmp

  do i=1,n
     tmp=a(i)
     a(i)=b(i)
     b(i)=tmp
  end do

end subroutine iswap

subroutine cswap(n,a,b)
  implicit none
  integer::i,n
  complex(8),intent(inout)::a(n),b(n)
  complex(8)::tmp

  do i=1,n
     tmp=a(i)
     a(i)=b(i)
     b(i)=tmp
  end do

end subroutine cswap

real(8) function MAC(n,r,c)
  implicit none
  integer::i
  integer,intent(in)::n
  real(8)::bunbo
  complex(8),intent(in)::r(n),c(n)
  complex(8)::rtmp,ctmp
  complex(8),parameter::zero=cmplx(0.d0,0.d0,kind=8)

  rtmp=zero;ctmp=zero
  do i=1,n
     rtmp=rtmp+r(i)
     ctmp=ctmp+c(i)
  end do
  if(rtmp.eq.zero.or.ctmp.eq.zero)then
     MAC=1.d0
     return
  end if


  ! if(minval(r).eq.zero.and.minval(c).eq.zero)then
  !    MAC=1.d0
  !    return
  ! end if
  bunbo=dot_product(r,r)*dot_product(c,c)

  if(bunbo.eq.0.d0)then         !r,cが零ベクトルであれば
     write(*,*) "error!@MAC"
     stop
  end if

  MAC=abs(dot_product(r,c))**2/bunbo

end function MAC

! subroutine impose_symmetry_avg(n,f)
!   integer::j
!   integer,intent(in)::n
!   real(8),intent(inout)::f(0:n,0:n)
!   real(8)::rtmp

!   ! impose the 1/8 symmetry
!   !naname
!   do j=1,n/2-1
!      rtmp=(f(n/2+j,n/2+j)+f(n/2-j,n/2+j)+f(n/2+j,n/2-j)+f(n/2-j,n/2-j))/4.d0
!      if(j.lt.n/2-1)then
!         f(n/2+j,n/2+j)=rtmp
!         f(n/2-j,n/2+j)=rtmp
!         f(n/2+j,n/2-j)=rtmp
!         f(n/2-j,n/2-j)=rtmp
!      else if(j.eq.n/2-1)then    !これはもういらない
!         f(n-1:n,n-1:n)=rtmp
!         f(0  :1,n-1:n)=rtmp
!         f(n-1:n,0  :1)=rtmp
!         f(0  :1,0  :1)=rtmp
!      end if
!   end do

!   !yoko
!   do j=1,n/2
!      if(j.lt.n/2)&
!           rtmp=(f(n/2,n/2+j)+f(n/2,n/2-j)+f(n/2+j,n/2)+f(n/2-j,n/2))/4.d0
!      f(n/2,n/2+j)=rtmp
!      f(n/2,n/2-j)=rtmp
!      f(n/2+j,n/2)=rtmp
!      f(n/2-j,n/2)=rtmp
!   end do
!   !naibu
!   do j=1,n/2-2
!      do i=j+1,n/2
!         if(i.lt.n/2)&
!              rtmp=(f(n/2+i,n/2+j)+f(n/2+i,n/2-j)+f(n/2+j,n/2+i)+&
!              f(n/2-j,n/2+i)+f(n/2-i,n/2+j)+f(n/2-i,n/2-j)+&
!              f(n/2+j,n/2-i)+f(n/2-j,n/2-i))/8.d0
!         f(n/2+i,n/2+j)=rtmp
!         f(n/2+i,n/2-j)=rtmp
!         f(n/2+j,n/2+i)=rtmp
!         f(n/2-j,n/2+i)=rtmp
!         f(n/2-i,n/2+j)=rtmp
!         f(n/2-i,n/2-j)=rtmp
!         f(n/2+j,n/2-i)=rtmp
!         f(n/2-j,n/2-i)=rtmp
!      end do
!   end do
! end subroutine impose_symmetry_avg

subroutine impose_symmetry_avg(n,f)
  integer::j
  integer,intent(in)::n
  real(8),intent(inout)::f(0:n,0:n)
  real(8)::rtmp

  ! impose the 1/8 symmetry
  !naname
  do j=1,n/2
     rtmp=(f(n/2+j,n/2+j)+f(n/2-j,n/2+j)+f(n/2+j,n/2-j)+f(n/2-j,n/2-j))/4.d0
     f(n/2+j,n/2+j)=rtmp
     f(n/2-j,n/2+j)=rtmp
     f(n/2+j,n/2-j)=rtmp
     f(n/2-j,n/2-j)=rtmp
  end do

  !yoko
  do j=1,n/2
     rtmp=(f(n/2,n/2+j)+f(n/2,n/2-j)+f(n/2+j,n/2)+f(n/2-j,n/2))/4.d0
     f(n/2,n/2+j)=rtmp
     f(n/2,n/2-j)=rtmp
     f(n/2+j,n/2)=rtmp
     f(n/2-j,n/2)=rtmp
  end do

  !naibu
  do j=1,n/2-1
     do i=j+1,n/2
        rtmp=(f(n/2+i,n/2+j)+f(n/2+i,n/2-j)+f(n/2+j,n/2+i)+&
             f(n/2-j,n/2+i)+f(n/2-i,n/2+j)+f(n/2-i,n/2-j)+&
             f(n/2+j,n/2-i)+f(n/2-j,n/2-i))/8.d0
        f(n/2+i,n/2+j)=rtmp
        f(n/2+i,n/2-j)=rtmp
        f(n/2+j,n/2+i)=rtmp
        f(n/2-j,n/2+i)=rtmp
        f(n/2-i,n/2+j)=rtmp
        f(n/2-i,n/2-j)=rtmp
        f(n/2+j,n/2-i)=rtmp
        f(n/2-j,n/2-i)=rtmp
     end do
  end do
end subroutine impose_symmetry_avg

subroutine impose_symmetry(n,f)
  integer::j
  integer,intent(in)::n
  real(8),intent(inout)::f(0:n,0:n)
  real(8)::rtmp

  ! impose the 1/8 symmetry
  !naname
  do j=1,n/2
     rtmp=f(n/2+j,n/2+j)
     f(n/2-j,n/2+j)=rtmp
     f(n/2+j,n/2-j)=rtmp
     f(n/2-j,n/2-j)=rtmp
  end do

  !yoko
  do j=1,n/2
     rtmp=f(n/2+j,n/2)
     f(n/2,n/2+j)=rtmp
     f(n/2,n/2-j)=rtmp
     f(n/2-j,n/2)=rtmp
  end do
  !naibu
  do j=1,n/2-1
     do i=j+1,n/2
        rtmp=f(n/2+i,n/2+j)
        f(n/2+i,n/2-j)=rtmp
        f(n/2+j,n/2+i)=rtmp
        f(n/2-j,n/2+i)=rtmp
        f(n/2-i,n/2+j)=rtmp
        f(n/2-i,n/2-j)=rtmp
        f(n/2+j,n/2-i)=rtmp
        f(n/2-j,n/2-i)=rtmp
     end do
  end do
end subroutine impose_symmetry
