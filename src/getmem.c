#include <sys/resource.h>


/* メモリ使用量取得(fortranから呼び出し) */
int
getmem_(int *mem)
{
  struct rusage ru;
  getrusage(RUSAGE_SELF, &ru);
  *mem = ru.ru_maxrss;

  return *mem;
}

/* メモリ使用量取得(cから呼び出し) */
int
getmem()
{
  int mem;
  struct rusage ru;
  getrusage(RUSAGE_SELF, &ru);
  mem = ru.ru_maxrss;

  return mem;
}
