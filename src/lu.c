#include <stdio.h>
/* #include "settings.h" */
#include "hmatrix.h"
/* #include "hcoarsen.h" */

/* #include "harith.h" */


/* HLU分解のwrapper */
void ludecomp_(phmatrix hmat, real *tol){
  /* printf(" # LU decompostion\n"); */
  /* omp_set_nested(1); */
  /* omp_set_dynamic(0); */


  /* lrdecomp_hmatrix_omp(hmat, 0, *tol);   */
  /* printf("@ludecomp:%d\n",hmat); */
  lrdecomp_hmatrix(hmat, 0, *tol);


}


/* 前進 & 後退代入のwrapper */
void lusolve_(uint *flag, phmatrix hmat, uint *brows, uint *bcols, field btmp[*bcols][*brows]){
  pavector bvec;
  pavector  v;
  /* pamatrix bmat; */
  /* pamatrix a; */
  bool atrans;
  uint i,j;
  /* FILE *fp; */

  /* printf("@lusolve :%d\n",hmat); */
  atrans = (*flag==1) ? true : false;
  
  v = (pavector) allocmem(sizeof(avector));
  /* a = (pamatrix) allocmem(sizeof(amatrix)); */
  /* bmat = init_amatrix(a,*brows,*bcols); */
  /* bmat->a=btmp; */
  bvec = init_avector(v, *brows);
  for(i=0;i<*bcols;i++){
    for(j=0;j<*brows;j++){
      bvec->v[j]=btmp[i][j];
    }
  /* printf("atrans:%d\n",atrans); */

  /* fp = fopen("bvec.res", "a"); */
  /* for(i=0;i<*edges;i++){ */
  /*   fprintf(fp,"%d %f\n",i+1,bvec->v[i]); */
  /* } */
  
    lrsolve_hmatrix_avector(atrans, hmat, bvec);

    for(j=0;j<*brows;j++){
      btmp[i][j]=bvec->v[j];
    }

  }
}

/* void lusolve_(uint *flag, phmatrix hmat, uint *edges, field *btmp){ */
/*   pavector bvec; */
/*   pavector  v; */
/*   bool atrans; */

/*   atrans = (*flag==1) ? true : false; */
/*   /\* printf("atrans:%d\n",atrans); *\/ */

/*   /\* printf(" # Solve \n"); *\/ */
/*   v = (pavector) allocmem(sizeof(avector)); */
/*   bvec = init_avector(v, *edges); */
/*   bvec->v=btmp; */
/*   lrsolve_hmatrix_avector(atrans, hmat, bvec); */
/* } */

