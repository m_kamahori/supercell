module m_hmatrix
  use iso_c_binding
  implicit none

  type,bind(c) :: cluster
     integer(c_int) :: size
     type(c_ptr) :: idx
     integer(c_int) :: sons
     type(c_ptr) :: pcluster
     integer(c_int) :: dim
     real(c_double) :: bmin
     real(c_double) :: bmax
     integer(c_int) :: desc
     integer(c_int) :: type
  end type cluster

  type,bind(c) :: hmatrix
     type(c_ptr) :: rc
     type(c_ptr) :: cc
     type(c_ptr) :: r
     type(c_ptr) :: f
     type(c_ptr) :: son
     integer(c_int) :: rsons
     integer(c_int) :: csons
     integer(c_int) :: refs
     integer(c_int) :: desc
  end type hmatrix

end module m_hmatrix
