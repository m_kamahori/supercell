subroutine kernelin(rows,cols,ridx,cidx,rorg,corg,drct,dflag)
  use globals
  use omp_lib
  implicit none
  integer :: i,j,ig,nj1,nj2,rows,cols,id,ib,jb,ik,jk
  integer :: ridx(rows),cidx(cols),rorg(rows),corg(cols),dflag
  real(8) :: x1,x2,y1,y2,leng
  real(8) :: r,rr1,rr2,xin,yin
  complex(8)::z
  complex(8)::CBJ0,CBJ1,CBY0,CBY1
  complex(8) :: zzcy(2),drct(rows,cols),stmp,dtmp,dstmp(2),ddtmp(2)

  ! drct(:,:)=zero

  id=omp_get_thread_num()
  do i=1,rows
     ib=ridx(i)+1
     xin=grid(1,ib)
     yin=grid(2,ib)
     do j=1,cols
        jb=cidx(j)+1

        ik=rorg(i)+1
        jk=corg(j)+1
        
        stmp=cmplx(0.d0,0.d0,kind=8)
        dtmp=cmplx(0.d0,0.d0,kind=8)
        dstmp(:)=cmplx(0.d0,0.d0,kind=8)
        ddtmp(:)=cmplx(0.d0,0.d0,kind=8)

        if(krnl(id)%flag(ik,jk).eq.0) then
           krnl(id)%flag(ik,jk)=1

           x1=(c(1,jb)-xin)*an(2,jb)-(c(2,jb)-yin)*an(1,jb)
           x2=(c(1,jb)-xin)*an(1,jb)+(c(2,jb)-yin)*an(2,jb)
        
           nj1=nd(1,jb)
           nj2=nd(2,jb)

           leng=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2)

           do ig=1,ng
              r=sqrt((x1-leng*gzai(ig))**2+x2**2)

              y1=(1+gzai(ig))*c(1,jb)-gzai(ig)*p(1,nj1)
              y2=(1+gzai(ig))*c(2,jb)-gzai(ig)*p(2,nj1)

              z=wn_g*r
              call CJY01_R(Z,CBJ0,CBJ1,CBY0,CBY1)

              zzcy(1)=CBJ0+ione*CBY0
              zzcy(2)=CBJ1+ione*CBY1

              stmp    =stmp    +wei(ig)*leng*zzcy(1)
              dtmp    =dtmp    +wei(ig)*leng*wn_g*zzcy(2)*(-x2)/r
              dstmp(1)=dstmp(1)-wei(ig)*leng*wn_g*zzcy(2)*(xin-y1)/r
              dstmp(2)=dstmp(2)-wei(ig)*leng*wn_g*zzcy(2)*(yin-y2)/r
              ddtmp(1)=ddtmp(1)+wei(ig)*wn_g**2*leng*zzcy(1)*an(1,jb)
              ddtmp(2)=ddtmp(2)+wei(ig)*wn_g**2*leng*zzcy(1)*an(2,jb)
           end do

           rr1=sqrt((xin-p(1,nj1))**2+(yin-p(2,nj1))**2)
           z=wn_g*rr1
           call CJY1_R(Z,CBJ1,CBY1)
           zzcy(1)=CBJ1+ione*CBY1

           rr2=sqrt((xin-p(1,nj2))**2+(yin-p(2,nj2))**2)
           z=wn_g*rr2
           call CJY1_R(Z,CBJ1,CBY1)
           zzcy(2)=CBJ1+ione*CBY1

           ddtmp(1)=ddtmp(1)+wn_g*(zzcy(2)*(yin-p(2,nj2))/rr2-zzcy(1)*(yin-p(2,nj1))/rr1)
           ddtmp(2)=ddtmp(2)-wn_g*(zzcy(2)*(xin-p(1,nj2))/rr2-zzcy(1)*(xin-p(1,nj1))/rr1)

           krnl(id)%s(ik,jk)  = stmp    *0.25d0*ione
           krnl(id)%d(ik,jk)  =-dtmp    *0.25d0*ione
           krnl(id)%ds1(ik,jk)= dstmp(1)*0.25d0*ione
           krnl(id)%ds2(ik,jk)= dstmp(2)*0.25d0*ione
           krnl(id)%dd1(ik,jk)=-ddtmp(1)*0.25d0*ione
           krnl(id)%dd2(ik,jk)=-ddtmp(2)*0.25d0*ione
        end if
        
        if(dflag.eq.1) then
           drct(i,j)=krnl(id)%s(ik,jk)
        elseif(dflag.eq.2) then
           drct(i,j)=krnl(id)%d(ik,jk)
        elseif(dflag.eq.3) then
           drct(i,j)=krnl(id)%ds1(ik,jk)
        elseif(dflag.eq.4) then
           drct(i,j)=krnl(id)%ds2(ik,jk)
        elseif(dflag.eq.5) then
           drct(i,j)=krnl(id)%dd1(ik,jk)
        elseif(dflag.eq.6) then
           drct(i,j)=krnl(id)%dd2(ik,jk)
        end if
     end do
  end do

end subroutine kernelin
