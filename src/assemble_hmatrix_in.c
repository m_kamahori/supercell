#include "harith.h"
#include <stdio.h>
#include <omp.h>

/* リーフブロックリスト構造体の定義 */
typedef struct _blist blist;
typedef blist* pblist;
struct _blist {
  uint bname; /* ブロック番号 */
  uint size;  /* 行数 x 列数 */
};

int sort2( const void * a , const void * b ) 
{
  return ((blist *)b)->size - ((blist *)a)->size;
}

extern void kernelin_(uint *rows, uint *cols, uint *ridx, uint *cidx, uint *rorg, uint *corg,
		      field *drct, uint *dflag);

extern void kernelin2_(uint *ii, uint *jj, uint *rows, uint *cols, uint *ridx, uint *cidx,
		       field *drct, uint *hrows, uint *hcols);

extern void alloc_kernelsin_(uint *rows, uint *cols, uint *id);

extern void dealloc_kernelsin_(uint *id);

extern void partacain(const uint *ridx, const uint rows, const uint *cidx, const uint cols,real accur,
		      uint **rpivot, uint **cpivot, uint dflag, pavector xvec,
		      uint xoff, uint yoff, pavector yvec);

void (*nearfield)(uint *rows, uint *cols, uint *ridx, uint *cidx, uint *rorg, uint *corg,
		  field *drct, uint *dflag);

void (*nearfield2)(uint *ii, uint *jj, uint *rows, uint *cols, uint *ridx, uint *cidx, 
		   field *drct, uint *hrows, uint *hcols); 

void cnt_leaf2(pcblock b, uint *nad, uint *ninad){
  pcblock b1;
  int i,j;
  int csons,rsons;
  if(b->son){
    rsons=b->rsons;
    csons=b->csons;
    for(j=0;j<csons;j++){
      for(i=0;i<rsons;i++){
	b1=b->son[i+j*rsons];
	cnt_leaf2(b1,nad,ninad);
      }
    }
  }
  else
    if(b->a>0){/* admissible */
      *nad+=1;
    }
    else{ /* inadmissible */
      *ninad+=1;
    }
}

//--------------------------------------------------------------------------------------------------//
void offsets(phmatrix hm, phmatrix *hn, uint *cnt, uint *xset, uint *yset, uint x0, uint y0){
  uint      rsons, csons;
  uint      xoff, yoff, i, j;

  if (hm->r) {
    xset[*cnt]=x0;
    yset[*cnt]=y0;
    hn[*cnt]=hm;
    *cnt+=1;
  }
  else if (hm->f) {
    xset[*cnt]=x0;
    yset[*cnt]=y0;
    hn[*cnt]=hm;
    *cnt+=1;
  }
  else {
    rsons = hm->rsons;
    csons = hm->csons;

    xoff = 0;
    for (j = 0; j < csons; j++) {

      yoff = 0;	
      for (i = 0; i < rsons; i++) {
	
 	offsets(hm->son[i+j*rsons], hn, cnt, xset, yset, x0+xoff, y0+yoff);

 	yoff += hm->son[i]->rc->size;
      }

      xoff += hm->son[j * rsons]->cc->size;
    }
  }
}


//--------------------------------------------------------------------------------------------------//
void assemble_hmatrix_in(pblock b, uint hrows, uint hcols, phmatrix G, real eps_aca, 
			 uint dflag[hrows*hcols],
			 field *xfwd, field *yfwd, uint nin, uint ne)
{
  uint i;
  phmatrix htmp,*hn;
  uint *ridx,*cidx;
  uint rows,cols;
  uint nad,ninad;
  uint ii,jj,j,k;
  field drct[6];

  nearfield=kernelin_;      /* 順・随伴の係数行列の計算 */
  nearfield2=kernelin2_;      /* 順・随伴の係数行列の計算 */

  pavector xpfwd;
  avector xtmp;
  uint ip;

  assert(ne == G->cc->size);
  assert(nin == G->rc->size);

  /* Permutation of x */
  xpfwd = init_avector(&xtmp, hcols*ne);
  for (i = 0; i < ne; i++) {
    ip = G->cc->idx[i];
    assert(ip < ne);
    for (j = 0; j < hcols; j++) {
      xpfwd->v[i+ne*j] = xfwd[ip+ne*j];
    }
  }

  uint cnt;
  uint x0, y0;

  /* リーフブロックのリストを作成 */
  nad=0;
  ninad=0;
  cnt_leaf2(b,&nad,&ninad);
  blist bl[nad+ninad];

  uint *xoffset;
  uint *yoffset;
  xoffset=allocmem(sizeof(uint)*(nad+ninad));
  yoffset=allocmem(sizeof(uint)*(nad+ninad));

  cnt=0;
  x0=0;
  y0=0;
  hn = allocmem((size_t) sizeof(phmatrix) * (nad+ninad));
  offsets(G, hn, &cnt, xoffset, yoffset, x0, y0);
  
  cnt=0;
  for (i=0;i<(nad+ninad);i++){
    if((hn[i]->r) || (hn[i]->f)){ /* admissilbe(r) or inadmissible(f) */
      rows=hn[i]->rc->size;
      cols=hn[i]->cc->size;
      bl[cnt].size=rows*cols;
      bl[cnt].bname=i;
      cnt+=1;
     }
  }

  /* サイズの大きい順に並び替え */
  qsort(bl,nad+ninad,sizeof(blist),sort2);

  /* ブロックサイズ順に行列要素を計算 */
  uint iomp;
  field *zfwd;
  pavector wfwd;
  avector wtmp;
  uint nomp;
#pragma omp parallel
  nomp=omp_get_num_threads();

  zfwd = allocmem(sizeof(field)*nomp*nin*hrows);
  for (i=0; i<nomp*nin*hrows; i++){
    zfwd[i]=0.0+I*0.0;
  }
  wfwd = init_zero_avector(&wtmp, hrows*nomp*nin);

  uint xoff,yoff;
#pragma omp parallel for private(rows, cols, ridx, cidx, htmp, ii, jj, i, j, k, iomp, xoff, yoff, drct) schedule(dynamic,1)
  for (i=0;i<(nad+ninad);i++) {
    iomp=omp_get_thread_num();
    htmp = hn[bl[i].bname];
    xoff = xoffset[bl[i].bname];
    yoff = yoffset[bl[i].bname];
    ridx = htmp->rc->idx;
    cidx = htmp->cc->idx;

    rows = htmp->rc->size;
    cols = htmp->cc->size;

    /* ACA */
    if (htmp->r) {
      alloc_kernelsin_(&rows,&cols,&iomp);
      for (j=0;j<hrows;j++) {
	for (k=0;k<hcols;k++) {
	  partacain(ridx, rows, cidx, cols, eps_aca, NULL, NULL, dflag[j+k*hrows],
		    xpfwd, xoff+k*ne, yoff+j*nin+iomp*hrows*nin, wfwd);
	}
      }
      dealloc_kernelsin_(&iomp);
    }
    else if (htmp->f) {
      for (ii=0;ii<rows;ii++) {
      	for (jj=0;jj<cols;jj++) {
      	  (*nearfield2)(&ii, &jj, &rows, &cols, ridx, cidx, drct, &hrows, &hcols);
      	  for (j=0;j<hrows;j++) {
      	    for (k=0;k<hcols;k++) {
      	      zfwd[ridx[ii]+j*nin+iomp*hrows*nin]+=drct[j+k*hrows]*xfwd[cidx[jj]+k*ne];
      	    }
      	  }
      	}
      }
    }
  }
  
  pavector yp;
  avector yptmp;
  yp = init_zero_avector(&yptmp, hrows*nin*nomp);
  /* Reverse permutation of y */
  for (i = 0; i < nin; i++) {
    for (j = 0; j < hrows; j++) {
      for (k = 0; k < nomp; k++){
	ip = G->rc->idx[i] + nin*j + hrows*nin*k;
	yp->v[ip] = wfwd->v[i+nin*j+hrows*nin*k];
      }
    }
  }

  for (i=0;i<nin;i++){
    for (j=0;j<hrows;j++) {
      for (k=0;k<nomp;k++) {
	yfwd[i+j*nin] += zfwd[i+j*nin+k*nin*hrows] + yp->v[i+j*nin+k*nin*hrows];
      }
    }
  }

  uninit_avector(yp);
  uninit_avector(xpfwd);
  uninit_avector(wfwd);
  freemem(zfwd);
  freemem(xoffset);
  freemem(yoffset);
  freemem(hn);
}
//--------------------------------------------------------------------------------------------------//
