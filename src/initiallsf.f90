subroutine initiallsf
  use globals
  use io
  implicit none
  
  integer :: ix,iy,i,j,itmp,ixx,iyy
  real(8) :: cx(2),dminx,dminy,d,tmp(0:xe,0:ye),rtmp,offset(2)
  ! real(8),allocatable :: lsf(:,:) !level set function
!!$----------------------one hole--------------------------------------------
  ! cx(:)=(avec(:,1)+avec(:,2))*0.5d0 !穴の中心の座標
  ! ! write(*,*) cx(:)
  ! open(10,file=file_lsf)
  ! do iy=0,ye
  !    do ix=0,xe

  !       x(:)=dble(ix)/dble(xe)*avec(:,1)+dble(iy)/dble(xe)*avec(:,2)
        
  !       dminx=x(1)-cx(1) !穴の中心と点(ix,iy)の距離のx成分
  !       dminy=x(2)-cx(2) !穴の中心と点(ix,iy)の距離のy成分
  !       d=sqrt(dminx*dminx+dminy*dminy)-holerad

  !       if(d>1d0)  d=1.d0
  !       if(d<-1d0) d=-1.d0
  !       ! if(ix.eq.0.or.ix.eq.xe.or.iy.eq.0.or.iy.eq.ye) d=0.d0 !境界上の点

  !       write(10,fmt_d3) x(:),d      !file_lsfへの書き出し

  !    end do
  ! end do
  ! close(10)
!!$----------------------one hole--------------------------------------------
! !!$----------------------two holes--------------------------------------------
!   cx(:)=(avec(:,1)+avec(:,2))*1.d0/3.d0 !三角形の重心
!   allocate(lsf(0:xe,0:ye))
!   do iy=0,ye
!      do ix=0,xe-iy
!         x(:)=dble(ix)/dble(xe)*avec(:,1)+dble(iy)/dble(xe)*avec(:,2)
        
!         dminx=x(1)-cx(1) !穴の中心と点(ix,iy)の距離のx成分
!         dminy=x(2)-cx(2) !穴の中心と点(ix,iy)の距離のy成分
!         d=sqrt(dminx*dminx+dminy*dminy)-holerad

!         if(d>1d0)  d=1.d0
!         if(d<-1d0) d=-1.d0
!         if(ix.eq.0.or.iy.eq.0) d=0.d0 !境界上の点

!         lsf(ix   ,iy   )=d
!         if(ix.eq.(xe-iy))cycle       !対称の軸での重複を回避
!         lsf(xe-iy,ye-ix)=d      !対称な点へコピー
!      end do
!   end do
  
!   open(10,file=file_lsf)
!   do iy=0,ye
!      do ix=0,xe
!         x(:)=dble(ix)/dble(xe)*avec(:,1)+dble(iy)/dble(xe)*avec(:,2)
!         write(10,fmt_d3) x(:),lsf(ix,iy)
!      end do
!   end do
!   close(10)

!   deallocate(lsf)
  
  ! !!$----------------------two holes--------------------------------------------
  
!!$----------------------no hole--------------------------------------------
  ! レベルセット関数の読み込み
  open(10,file=file_lsfex)
  do iy=0,ye
     do ix=0,xe
        read(10,*) rtmp,rtmp,tmp(ix,iy)
     end do
  end do
  close(10)
  if(rtmp < 1.d0)stop "Please set the parameter xe collectly"
  offset(:)=-(uvec(:,1)+uvec(:,2))*(ncell-1)/2 !offset
  nip=0
  do iy=0,ye
     do ix=0,xe
        do j=0,ncell-1
           do i=0,ncell-1
              ixx=xmin+i*xe+ix
              iyy=ymin+j*ye+iy
              xlsf(:,ixx,iyy)=offset(:)
              xlsf(:,ixx,iyy)=xlsf(:,ixx,iyy)+i*uvec(:,1)+j*uvec(:,2)
              xlsf(:,ixx,iyy)=xlsf(:,ixx,iyy)+ix/dble(xe)*uvec(:,1)+dble(iy)/dble(ye)*uvec(:,2)
              if(ixx > 1.and.ixx < xe-1.and.iyy > 1.and.iyy < ye-1)then
                 lsfunc(ixx,iyy)=0.1d0 !中心セルは誘電体領域
              else
                 lsfunc(ixx,iyy)=tmp(ix,iy)
              end if
              ! lsfunc(ixx,iyy)=tmp(ix,iy)  !中心セルもexternal unit cellと同じ
           end do
        end do
     end do
  end do
  open(10,file=file_lsfspr)     !lsf of supercell
  do iyy=ymin,ymax
     do ixx=xmin,xmax
        write(10,fmt_d3) xlsf(:,ixx,iyy),lsfunc(ixx,iyy)
     end do
  end do
  close(10)
  open(10,file=file_lsf)        !lsf of design domain
  do iy=0,ye
     do ix=0,xe
        write(10,fmt_d3) xlsf(:,ix,iy),lsfunc(ix,iy)
     end do
  end do
  close(10)
!!$----------------------no hole--------------------------------------------

end subroutine initiallsf

subroutine initiallsf2
  use globals
  use io
  implicit none
  
  integer :: ix,iy,k
  real(8) :: cx(2),dminx,dminy,d(4)
  real(8) :: x(2) !格子点
  ! real(8),allocatable :: lsf(:,:) !level set function
!!$----------------------one hole--------------------------------------------
  ! write(*,*) cx(:)
  open(10,file=file_lsf)
  do iy=0,ye
     do ix=0,xe

        x(:)=dble(ix)/dble(xe)*avec(:,1)+dble(iy)/dble(xe)*avec(:,2)

        do k=1,4
           if(k.eq.1.or.k.eq.2)then
              cx(:)=avec(:,k)*0.5d0
           else if(k.eq.3.or.k.eq.4)then
              cx(:)=avec(:,mod(k,2)+1)*0.5d0+k/3*avec(:,k-2)
           end if
           dminx=x(1)-cx(1) !穴の中心と点(ix,iy)の距離のx成分
           dminy=x(2)-cx(2) !穴の中心と点(ix,iy)の距離のy成分
           d(k)=sqrt(dminx*dminx+dminy*dminy)
           if(d(k)>1d0)  d(k)=1.d0
           if(d(k)<-1d0) d(k)=-1.d0
        end do

        ! if(ix.eq.0.or.ix.eq.xe.or.iy.eq.0.or.iy.eq.ye) d=0.d0 !境界上の点

        write(10,fmt_d3) x(:),minval(d)-holerad      !file_lsfへの書き出し

     end do
  end do
  close(10)
!!$----------------------one hole--------------------------------------------
end subroutine initiallsf2
