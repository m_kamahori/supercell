#include "harith.h"
#include <stdio.h>

extern void (*nearfield)(uint *rows, uint *cols, uint *ridx, uint *cidx, uint *rorg, uint *corg,
			 field *drct, uint *dflag);


void partacain(const uint *ridx, const uint rows, const uint *cidx, const uint cols,real accur,
	       uint **rpivot, uint **cpivot, uint dflag, pavector xvec,
	       uint xoff, uint yoff, pavector yvec){

  pamatrix A, B;
  amatrix A_k, B_k;
  uint *rperm, *cperm, *rpiv, *cpiv, *rorg, *corg;
  uint i, j, mu, k, i_k, j_k;
  uint dlprows,dlpcols;
  real error, error2, starterror, M;
  field Aij;
  pfield aa, bb;

  //  printf("creal(xpfwd)=%e\n",xvec->a[0]);
  //  puts("===in partacain.c")

  k = 0;
  rperm = allocmem(rows * sizeof(uint));
  cperm = allocmem(cols * sizeof(uint));
  rpiv = allocmem(rows * sizeof(uint));
  cpiv = allocmem(cols * sizeof(uint));
  rorg = allocmem(rows * sizeof(uint));
  corg = allocmem(cols * sizeof(uint));

  /* pivot初期化 */
  for (i = 0; i < rows; ++i) {  
    rpiv[i] = ridx[i];
  }

  /* pivot初期化 */
  for (j = 0; j < cols; ++j) {
    cpiv[j] = cidx[j];
  }

  /* org初期化 */
  /* いまi番の行はもとはrorg[i]番 */
  for (i = 0; i < rows; ++i) {
    rorg[i] = i;
  }

  /* org初期化 */
  /* いまj番の列はもとはcorg[j]番 */
  for (j = 0; j < cols; ++j) {
    corg[j] = j;
  }

  A = new_amatrix(rows, k);
  B = new_amatrix(cols, k);

  error = 1.0;
  starterror = 1.0;

  /* IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII */
  /* ここからACA */
  /* D->AとBを計算する (D->A x B^T が近似行列)*/
  /* IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII */
  while (error > accur && k < rows && k < cols) { /* 許容誤差を下回るか、フルランクとなるまで繰り返す  */
    resizecopy_amatrix(A, rows, k + 1); /* D->Aのサイズ1up  */
    resizecopy_amatrix(B, cols, k + 1); /* Bのサイズ1up  */

    /* aa,bbはこれまでのステップで計算してきた近似行列構造体 */
    aa = A->a; 
    bb = B->a;

    (void) init_sub_amatrix(&A_k, A, rows - k, k, 1, k);
    (void) init_sub_amatrix(&B_k, B, cols - k, k, 1, k);
    /* A_k はこのステップで計算する A の部分行列構造体/ */
    /* B_k はこのステップで計算する B の部分行列構造体/ */
    /* ポインタで参照してるのでA_k,B_kに対する計算はそのままaa,bbとかD_A,D_Bに反映される */

    if(k>0){
      /* Compute next i_k. */
      M = cabs(aa[k + (k - 1) * rows]);
      i_k = k;
      for (i = k + 1; i < rows; ++i) {
     	if (cabs(aa[i + (k - 1) * rows]) > M) {
     	  M = cabs(aa[i + (k - 1) * rows]);
     	  i_k = i;
	}
      } 
    } 
    else { 
      i_k = rows / 2;
    }

    /* Get next column for B.*/
    dlprows = 1;
    dlpcols = cols - k;
    (*nearfield)(&dlprows, &dlpcols, rpiv + i_k, cpiv + k, rorg + i_k, corg + k, B_k.a, 
		 &dflag);

    /* Subtract current rank-k-approximation. */
    for (j = 0; j < k; ++j) {
      bb[j + k * cols] = 0.0;
    }
    for (mu = 0; mu < k; ++mu) {
      Aij = aa[i_k + mu * rows]; 
      for (j = k; j < cols; ++j) {
	bb[j + k * cols] = bb[j + k * cols] - bb[j + mu * cols] * Aij;
      } 
    }

    /* Compute next j_k. */
    M = cabs(bb[k + k * cols]);
    j_k = k;
    for (j = k + 1; j < cols; ++j) {
      if (cabs(bb[j + k * cols]) > M) {
    	M = cabs(bb[j + k * cols]);
	j_k = j;
      }
    }

    /* Get next column for A. */
    dlprows = rows - k;
    dlpcols = 1;
    (*nearfield)(&dlprows, &dlpcols, rpiv + k, cpiv + j_k, rorg + k, corg + j_k, A_k.a, 
		 &dflag);


    /* Subtract current rank-k-approximation. */
    for (i = 0; i < k; ++i) {
      aa[i + k * rows] = 0.0;
    }
    for (mu = 0; mu < k; ++mu) {
      Aij = bb[j_k + mu * cols];
      for (i = k; i < rows; ++i) {
    	aa[i + k * rows] = aa[i + k * rows] - aa[i + mu * rows] * Aij;
      }
    }

    /* 正規化 */
    Aij = 1.0 / bb[j_k + k * cols];
    for (i = 0; i < rows; ++i) {
      aa[i + k * rows] = aa[i + k * rows] * Aij;
    }

    /* Update permutations.*/
    i = rpiv[k];
    rpiv[k] = rpiv[i_k];
    rpiv[i_k] = i;
    rperm[k] = i_k;

    j = cpiv[k];
    cpiv[k] = cpiv[j_k];
    cpiv[j_k] = j;
    cperm[k] = j_k;

    i=rorg[k];
    rorg[k] = rorg[i_k];
    rorg[i_k] = i;

    j=corg[k];
    corg[k] = corg[j_k];
    corg[j_k] = j;

    k++;

    /* Apply permutation to current approximation */
    for (i = 0; i < k; ++i) {
      Aij = aa[i_k + i * rows];
      aa[i_k + i * rows] = aa[k - 1 + i * rows];
      aa[k - 1 + i * rows] = Aij;
    }

    for (j = 0; j < k; ++j) {
      Aij = bb[j_k + j * cols];
      bb[j_k + j * cols] = bb[k - 1 + j * cols];
      bb[k - 1 + j * cols] = Aij;
    }

    /* 1ステップ目の誤差評価 */
    if (k == 1) {
      /* Computation of starterror. */
      starterror = 0.0; /* 行ベクトルの大きさ */
      for (i = k - 1; i < rows; ++i) {
    	starterror += ABSSQR(aa[i]);
      }
      error = 0.0; /* 列ベクトルの大きさ */
      for (j = k - 1; j < cols; ++j) {
    	error += ABSSQR(bb[j]);
      }
      starterror = 1.0 / REAL_SQRT(starterror * error); /* 1ステップ目の誤差 (= 行、列ベクトルのFrobeniusノルムの積) */
    }


    /* Computation of current relative error. */
    error = 0.0; /* 行ベクトルの大きさ */
    for (i = k - 1; i < rows; ++i) {
      error += ABSSQR(aa[i + (k - 1) * rows]);
    }
    error2 = 0.0; /* 列ベクトルの大きさ */
    for (j = k - 1; j < cols; ++j) {
      error2 += ABSSQR(bb[j + (k - 1) * cols]);
    }
    error = REAL_SQRT(error * error2) * starterror; /* kステップ目の相対誤差 */

    uninit_amatrix(&A_k);
    uninit_amatrix(&B_k);
    
  }
  /* IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII */
  /* ここまでACA */
  /* IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII */


  /* Reverse pivot permutations */
  aa = A->a;
  bb = B->a;
  for (i = k; i-- > 0;) {
    for (j = 0; j < k; j++) {
      Aij = aa[i + j * rows];
      aa[i + j * rows] = aa[rperm[i] + j * rows];
      aa[rperm[i] + j * rows] = Aij;

      Aij = bb[i + j * cols];
      bb[i + j * cols] = bb[cperm[i] + j * cols];
      bb[cperm[i] + j * cols] = Aij;
    }
  }

  conjugate_amatrix(B);

  if (rpivot != NULL) {
    *rpivot = allocuint(k);
    for (i = 0; i < k; ++i) {
      (*rpivot)[i] = rpiv[i];
    }
  }

  if (cpivot != NULL) {
    *cpivot = allocuint(k);
    for (i = 0; i < k; ++i) {
      (*cpivot)[i] = cpiv[i];
    }
  }
  
  field alpha=1.0+0.0*I;
  pavector x1,y1;
  avector xtmp, ytmp;

  x1 = init_sub_avector(&xtmp, xvec, cols, xoff);
  y1 = init_sub_avector(&ytmp, yvec, rows, yoff);

  prkmatrix r;
  r = new_rkmatrix(rows, cols, k);
  r->A=*A;
  r->B=*B;
  
  assert(x1->dim == cols);
  assert(y1->dim == rows);
  addeval_rkmatrix_avector(alpha, r, x1, y1);

  del_amatrix(A);
  del_amatrix(B);
  freemem(rperm);
  freemem(cperm);
  freemem(rpiv);
  freemem(cpiv);
}
