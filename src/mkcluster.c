/* 境界クラスターを生成 */

#include "curve2d.h"
#include "bem2d.h"
#include "./H2Lib-2.0/Library/settings.h"

/* visualise structure of a cluster */
/* --------------------------------------------------------------------------------------------
 * For debug
 * --------------------------------------------------------------------------------------------*/
void check_cluster(pcluster root, uint level, FILE *fp){
  pcluster root1;
  uint i;
  if (root->son) { /* 子供がいれば */
    /* fprintf(fp,"sons=%d\n",root->sons); */
    fprintf(fp,"%15.11f %15.11f %d\n",root->bmax[0],root->bmax[1],level);
    fprintf(fp,"%15.11f %15.11f %d\n",root->bmax[0],root->bmin[1],level);
    fprintf(fp,"%15.11f %15.11f %d\n",root->bmin[0],root->bmin[1],level);
    fprintf(fp,"%15.11f %15.11f %d\n",root->bmin[0],root->bmax[1],level);
    fprintf(fp,"%15.11f %15.11f %d\n",root->bmax[0],root->bmax[1],level);
    fprintf(fp,"\n");
    fprintf(fp,"\n");
    
    for (i = 0 ; i < root->sons ; i++) {
      root1 = root->son[i];
      level = level + 1;
      check_cluster(root1,level,fp);
      level = level - 1;
    }
  }
  else {
    fprintf(fp,"%15.11f %15.11f %d\n",root->bmax[0],root->bmax[1],level);
    fprintf(fp,"%15.11f %15.11f %d\n",root->bmax[0],root->bmin[1],level);
    fprintf(fp,"%15.11f %15.11f %d\n",root->bmin[0],root->bmin[1],level);
    fprintf(fp,"%15.11f %15.11f %d\n",root->bmin[0],root->bmax[1],level);
    fprintf(fp,"%15.11f %15.11f %d\n",root->bmax[0],root->bmax[1],level); 
    fprintf(fp,"\n");
    fprintf(fp,"\n");
  }
}
/* --------------------------------------------------------------------------------------------
 * For debug
 * --------------------------------------------------------------------------------------------*/

pcluster
mkcluster(uint edges,		/* 要素数 */
	  uint vertices,	/* 節点数 */
	  double x[vertices][2],		/* x[i][j]: 第i節点のj+1座標 */
	  uint nd[vertices][2],		/* 要素 */
	  uint nmin,		/* ACAのnmin */
	  double an[vertices][2],		/* 法線ベクトル  */
	  double lngth[vertices])	/* 要素長 */
{
  int i;
  pcurve2d gr; /* 要素情報 */
  pcluster root; /* クラスタ情報 */
  pbem2d bem;
  int dbg;
  FILE *file;

#ifdef MODE_DBG
  dbg=1;
#else
  dbg=0;
#endif

  /* c用にインデックスをシフト */
  for (i=0;i<edges;i++){
    nd[i][0]-=1;
    nd[i][1]-=1;
  }

  /* gr(構造体)を生成 */
  gr=new_curve2d(edges,edges); /* grの中の配列をallocateし、verticesとedgeをgrにぶら下げる */
  gr->x=x;                       /* 節点座標 */
  gr->e=nd;                      /* 節点番号 */
  gr->n=an;                      /* 法線ベクトル */
  gr->g=lngth;                   /* 要素長  */
  gr->vertices = vertices;
  gr->edges = edges;

  /* for(i=0;i<*edges;i++){ */
  /*   printf("%u %u\n",nd[i][0],nd[i][1]); */
  /* } */
  /* printf("edges=%u\n",*edges); */

  if(dbg){
    file=fopen("x.dat","a");
    for (i=0;i<edges;i++) {
      fprintf(file,"%20.12e%20.12e %d %d\n",gr->x[gr->e[i][0]][0],gr->x[gr->e[i][0]][1],gr->e[i][0],gr->e[i][1]);
    }
     fprintf(file,"\n");
     fclose(file);
  }

  /* bem構造体を生成(build_bem2d_clusterを呼ぶのに必要) */
  bem = new_bem2d(gr); /* bem(構造体)にgrをぶら下げ、その他のmemberを初期化 */
  /* assert(bem->sq==NULL); */

  /* クラスターツリーを生成 */
  root = build_bem2d_cluster(bem,nmin,BASIS_CONSTANT_BEM2D); /* bem2d.c */

  
  /* fortran用にインデックスをシフト */
  for (i=0;i<edges;i++) {
    nd[i][0]+=1;
    nd[i][1]+=1;
  }

  if(dbg){			/* cluster check */
    FILE *fp;
    fp = fopen("root.res", "a");
    check_cluster(root, 0, fp);
    fclose(fp);
  }

  /* del_curve2d(gr); */
  /* printf("bem=%08X\n",bem); */
  /* del_bem2d(bem); */


  return root;
}

void w_check_cluster(pcluster root){
  FILE *fp;
  fp = fopen("root.res", "a");
  check_cluster(root, 0, fp);
  fclose(fp);
}
