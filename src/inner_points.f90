subroutine inner_points(u,q,k,ibril)
  use omp_lib
  use globals
  implicit none
  integer,intent(in)::k,ibril
  complex(8),intent(in)::u(ne),q(ne)
  integer::i,j,nj1,nj2,ig
  real(8)::xin,yin,x1,x2,a,rr,y1,y2
  complex(8)::stmp,dtmp,dstmp1,dstmp2,ddtmp1,ddtmp2,z,cy(2)
  complex(8)::CBJ0,CDJ0,CBJ1,CDJ1,CBY0,CDY0,CBY1,CDY1
  real(8)::rr1,rr2

  ugrid(:,k,ibril)=zero
  dugrid(:,:,k,ibril)=zero

  !$omp parallel do private(xin,yin,x1,x2,stmp,dtmp,dstmp1,dstmp2,ddtmp1,ddtmp2,&
  !$omp & nj1,nj2,a,rr,y1,y2,z,cbj0,cdj0,cbj1,cdj1,cby0,cdy0,cby1,cdy1,cy,rr1,rr2)
  do i=1,nip
     xin=grid(1,i)
     yin=grid(2,i)

     do j=1,ne
        x1=(xin-c(1,j))*(-an(2,j))+(yin-c(2,j))*an(1,j)
        x2=(xin-c(1,j))*an(1,j)+(yin-c(2,j))*an(2,j)

        stmp=zero
        dtmp=zero
        dstmp1=zero
        dstmp2=zero
        ddtmp1=zero
        ddtmp2=zero

        nj1=nd(1,j)
        nj2=nd(2,j)


        a=0.5*sqrt((p(1,nj2)-p(1,nj1))**2+(p(2,nj2)-p(2,nj1))**2)       

        do ig=1,ng
           rr=sqrt((x1-a*gzai(ig))**2+x2**2)
           y1=(1+gzai(ig))*c(1,j)-gzai(ig)*p(1,nj1)
           y2=(1+gzai(ig))*c(2,j)-gzai(ig)*p(2,nj1)

           z=wn_g*rr
           call CJY01(Z,CBJ0,CDJ0,CBJ1,CDJ1,CBY0,CDY0,CBY1,CDY1)
           cy(1)=CBJ0+ione*CBY0
           cy(2)=CBJ1+ione*CBY1

           stmp  =  stmp+a*wei(ig)*cy(1) !first layer potential
           dtmp  =  dtmp+a*wei(ig)*wn_g*cy(2)*x2/rr ! second layer potential
           dstmp1=dstmp1-a*wei(ig)*wn_g*cy(2)*(xin-y1)/rr
           dstmp2=dstmp2-a*wei(ig)*wn_g*cy(2)*(yin-y2)/rr
           ddtmp1=ddtmp1+wn_g**2*wei(ig)*a*cy(1)*an(1,j)
           ddtmp2=ddtmp2+wn_g**2*wei(ig)*a*cy(1)*an(2,j)
        enddo ! gauss


        rr1=sqrt((xin-p(1,nj1))**2+(yin-p(2,nj1))**2)
        z=wn_g*rr1
        call CJY01(Z,CBJ0,CDJ0,CBJ1,CDJ1,CBY0,CDY0,CBY1,CDY1)
        cy(1)=CBJ1+ione*CBY1

        rr2=sqrt((xin-p(1,nj2))**2+(yin-p(2,nj2))**2)
        z=wn_g*rr2
        call CJY01(Z,CBJ0,CDJ0,CBJ1,CDJ1,CBY0,CDY0,CBY1,CDY1)
        cy(2)=CBJ1+ione*CBY1

        ddtmp1=ddtmp1+wn_g*(cy(2)*(yin-p(2,nj2))/rr2-cy(1)*(yin-p(2,nj1))/rr1)
        ddtmp2=ddtmp2-wn_g*(cy(2)*(xin-p(1,nj2))/rr2-cy(1)*(xin-p(1,nj1))/rr1)
        ugrid(   i,k,ibril)= ugrid(  i,k,ibril)+(stmp*q(j)-dtmp*u(j))*0.25d0*ione
        dugrid(1,i,k,ibril)=dugrid(1,i,k,ibril)+(dstmp1*q(j)-ddtmp1*u(j))*0.25d0*ione
        dugrid(2,i,k,ibril)=dugrid(2,i,k,ibril)+(dstmp2*q(j)-ddtmp2*u(j))*0.25d0*ione
     end do ! j (elem)
  end do ! i (obs)
  !$omp end parallel do

end subroutine inner_points
