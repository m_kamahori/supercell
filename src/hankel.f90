subroutine hankel(flag,zr,zi,m,n,kotae)
  use omp_lib
  implicit none
  
  integer    :: kode,nz,ierr,i,m,n,flag
  complex(8) :: kotae(0:n-1),ione
  complex(8)::CBJ0,CDJ0,CBJ1,CDJ1,CBY0,CDY0,CBY1,CDY1
  real(8)    :: zr,zi,fnu,pi
  real(8),allocatable :: cyr_h(:),cyi_h(:),cyr_j(:),cyi_j(:)

  ! allocate(cyr_h(0:n-1),cyi_h(0:n-1),cyr_j(0:n-1),cyi_j(0:n-1))

  ione=cmplx(0.d0,1.d0,kind=8)
  pi=acos(-1.d0)

  ! fnu=0.d0
  ! kode=1

  ! call zbesh(zr,zi,fnu,kode,m,n,cyr_h,cyi_h,nz,ierr)
  ! call zbesj(zr,zi,fnu,kode,n,cyr_j,cyi_j,nz,ierr)

  ! do i=0,n-1
  !    if(flag.eq.1) then
  !       kotae(i)=cyr_h(i)+ione*cyi_h(i)
  !    else if(flag.eq.2) then
  !       kotae(i)=cyr_j(i)+ione*cyi_j(i)
  !    end if
  ! end do

  ! write(*,*) 'zbes'
  ! write(*,*) kotae(0)
  ! write(*,*) kotae(1)

  call cjy01(zr+ione*zi,CBJ0,CDJ0,CBJ1,CDJ1,CBY0,CDY0,CBY1,CDY1)

  if(flag.eq.1) then
     kotae(0)=CBJ0+ione*CBY0
     kotae(1)=CBJ1+ione*CBY1
  else if(flag.eq.2) then
     kotae(0)=CBJ0
     kotae(1)=CBJ1
  end if
  
  ! write(*,*) 'cjy01'
  ! write(*,*) kotae(0)
  ! write(*,*) kotae(1)

  ! deallocate(cyr_h,cyi_h,cyr_j,cyi_j)

end subroutine hankel
