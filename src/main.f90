program main
  use globals
  use io
  implicit none
  integer::i,iflag_bounded,itmp
  real(8),parameter::semicon=0.2d0,tsdmin=0.1d0
  integer::sflg=1                !flag of sensitivity. 1:shape 2:topological derivative
  real(8)::st,vt,tconv,tsd=0.d0,rtmp
  logical::flag

  call initialsettings

  allocate(hstry(0:step2))
  hstry(:)%flag=0
  file_hist=trim(trim(file_dir)//"/history.dat")
  open(11,file=file_hist)
  do i=0,step1-1
     if(opt_flag.eq.1)then
        read(11,*) itmp,hstry(i)%f,vt,tsd
     elseif(opt_flag.eq.-1)then
        read(11,*) itmp,hstry(i)%J,vt,tsd
     end if
     ! if(sflg.eq.1)then
     !    tsd
     ! else
     !    sdcnt=0
     ! end if
  end do
  do i=step1,step2
     ! vt=step2time(i)
     vt=update_time(i,vt)
     write(*,*)"# Step:",i
     write(*,*)"# Virtual time:",vt

     call convergence_test(flag,i,tconv) !収束判定
     if(flag.eqv..true.)exit

     tins=0.d0
     call checktime(tins,tint)
     st=tins

     call set_io_filename(i)

     if(i.eq.0) then
        if(mflag.eq.1)then
           call initiallsf         !初期形状のレベルセット関数を生成
           call lsf2elm(i,iflag_bounded)
        end if
     else
        call lsf2elm(i,iflag_bounded)
     end if


     call SSM(i,iflag_bounded)
     if(ncell.eq.1)then
        write(11,fmt_i1d3) i,hstry(i)%f,vt,tsd          !history of the full bandgap width
     else
        write(11,fmt_i1d3) i,hstry(i)%J,vt,tsd          !history of the objective function
     end if

     call checktime(tins,tint)
     write(*,*)"# tsd/tsdmin   ",tsd/tsdmin
     write(*,*)"# tconv/semicon",tconv/semicon
     if(i.eq.0)then
        write(*,*) "# Calculating topological derivatives"
        sflg=0
        call topological_derivative2
        ! call topological_derivative
        tsd=0.d0
     elseif(tsd >= tsdmin.and.tconv >= semicon)then
        write(*,*) "# Calculating topological derivatives"
        sflg=2
        call topological_derivative2
        ! call topological_derivative
        tsd=0.d0
     else
        sflg=1
        write(*,*) "# Calculating tangential components"
        call tan_comp2
        call checktime(tins,tint) 
        write(*,*) "# Calculating shape derivatives"
        call shape_derivative2
        tsd=update_time(i,tsd)
     end if
     call checktime(tins,tint) 
 
     write(*,*)"# Lsf evolution" 
     if(mflag.eq.0)stop "@no lsf file" 
     call update_lsf(sflg,i) 
     call checktime(tins,tint) 
 
     call checktime(st,tint) 
     time(3)=tint              !1stepの時間 
     open(22,file="time.dat",access='append') 
     write(22,'(I4,4e24.16,2I3,I4)') ne,time(3),time(1),time(2),edges(1)/dble(sum(edges(2:5))),islv,i 
     close(22) 

     deallocate(p,nd,an,at,c,lngth)
     deallocate(l2epre,e2lpre,nd4at,orgat,ltc2nd,l2e,e2l)
     call deallocate_ev(nbril,ev)
  end do
  close(11)

  if(allocated(ev_slc).eqv..true.)deallocate(ev_slc,evv_ref)
  stop

contains
  subroutine convergence_test(flag,step,dt)
    real(8),parameter::conv=5.d0
    logical,intent(out)::flag
    integer,intent(in)::step
    integer::s
    real(8),intent(out)::dt

    if(step > 0)then
       if(opt_flag.eq.1)then
          s=step_of_the_max_gap(step) !maxmisation
       elseif(opt_flag.eq.-1)then
          s=step_of_the_min_J(step) !minimisation
       else
          stop "flag has to be 1 or -1."
       end if
       dt=step2time(step)-step2time(s)
       write(*,*)dt,step2time(step),step2time(s)
       if(dt >= conv)then
          write(*,*) "# Full bandgap width is Converged."
          flag=.true.
       else
          flag=.false.
       end if
    else
       dt=0.d0;flag=.false.
    end if
    
  end subroutine convergence_test

  integer function steps_from_the_max_gap(en)
    integer::loc(1)
    integer,intent(in)::en

    loc(:)=maxloc(hstry(0:en-1)%f)
    steps_from_the_max_gap=en-(loc(1)-1)

  end function steps_from_the_max_gap

  integer function step_of_the_max_gap(en)
    integer::loc(1)
    integer,intent(in)::en

    loc(:)=maxloc(hstry(0:en-1)%f)
    step_of_the_max_gap=loc(1)-1

  end function step_of_the_max_gap

  integer function step_of_the_min_J(en)
    integer::loc(1)
    integer,intent(in)::en

    loc(:)=minloc(hstry(0:en-1)%J)
    step_of_the_min_J=loc(1)-1

  end function step_of_the_min_J
  
  real(8) function step2time(ii)
    integer,intent(in)::ii

    if(ii < tloop2)then
       step2time=deltat1*ii
    else
       step2time=deltat1*tloop2+deltat2*(ii-tloop2)
    end if

  end function step2time

  real(8) function update_time(ii,t)
    integer,intent(in)::ii
    real(8),intent(in)::t

    if(ii < tloop2)then
       update_time=t+deltat1
    else
       update_time=t+deltat2
    end if

  end function update_time

  subroutine deallocate_ev(n,ev)
    integer::i
    integer,intent(in)::n
    type(eigenpairs)::ev(n)

    do i=1,n
       deallocate(ev(i)%s)
       deallocate(ev(i)%v)
       deallocate(ev(i)%u)
       deallocate(ev(i)%un)
       deallocate(ev(i)%ut)
       deallocate(ev(i)%uin)
       deallocate(ev(i)%duin)
    end do
  end subroutine deallocate_ev
    
end program main
