subroutine solver(idomp,ibril,nirep,nperi,vmat,ymat,islv)
  integer,intent(in)::ibril,nirep,nperi,idomp,islv
  complex(8),intent(in)::vmat(:,:)
  complex(8),intent(out)::ymat(:,:)

  if(islv.eq.-1)then
     call lu(idomp,ibril,vmat,ymat)
  else if(islv.eq.0)then
     call woodbury(idomp,ibril,nirep,nperi,vmat,ymat)
  else if(islv.eq.2)then
     call hwoodbury(idomp,ibril,nirep,nperi,vmat,ymat)
  else if(islv.eq.3)then
     call hlu(idomp,ibril,vmat,ymat)
  end if

contains
  subroutine lu(idomp,ibril,vmat,ymat)
    use globals
    implicit none
    integer,intent(in)::ibril,idomp
    integer::info
    integer,allocatable::ipiv(:)
    complex(8),intent(in)::vmat(:,:)
    complex(8),intent(out)::ymat(:,:)

    allocate(ipiv(ne))

    call bem_matrix_lu(ibril,idomp)

    ymat=vmat
    !A^{-1}v
    call zgesv(ne,nblck,amat(:,:,idomp),ne,ipiv(:),ymat(:,:),ne,info) 

    deallocate(ipiv)

  end subroutine lu
    
  subroutine woodbury(idomp,ibril,nirep,nperi,vmat,ymat)
    use globals
    implicit none
    integer,intent(in)::ibril,nirep,nperi,idomp
    integer::info
    integer,allocatable::ipiv_d(:),ipiv(:)
    complex(8),allocatable::fwork(:,:)
    complex(8),allocatable::gwork(:,:),ework(:,:)
    complex(8),allocatable::subd(:,:),sube(:,:),sube_lu(:,:)
    complex(8),intent(in)::vmat(:,:)
    complex(8),intent(out)::ymat(:,:)

    interface
       subroutine bem_matrix(subd,sube,ibril,s,d)
         use globals
         implicit none
         integer,intent(in)::ibril
         complex(8),intent(out)::subd(:,:),sube(:,:)
         complex(8),intent(in)::s(:,:),d(:,:)
       end subroutine bem_matrix
    end interface

    allocate(fwork(nirep,nblck),gwork(nperi,nblck),ework(nperi,nperi),&
         subd(nirep,nperi),sube(nperi,nperi),sube_lu(nperi,nperi),ipiv_d(nperi),ipiv(nperi))
    
    call bem_matrix(subd(:,:),sube(:,:),ibril,smat(:,:,idomp),dmat(:,:,idomp))
    sube_lu(:,:)=sube(:,:)

    gwork(:,:)=vmat(nirep+1:ne,:)
    !E^{-1}g
    call zgesv(nperi,nblck,sube_lu(:,:),nperi,ipiv_d(:),gwork(:,:),nperi,info) 

    !f-DE^{-1}g
    fwork(:,:)=vmat(1:nirep,:)
    call zgemm("N","N",nirep,nblck,nperi,-one,subd,nirep,gwork,nperi,one,fwork,nirep)

    !CB^{-1}(f-DE^{-1}g)
    call zgemm("N","N",nperi,nblck,nirep,one,cbinv(:,:,idomp),nperi,fwork,nirep,zero,gwork,nperi)

    !E-CB^{-1}D
    ework(:,:)=sube(:,:)
    call zgemm("N","N",nperi,nperi,nirep,-one,cbinv(:,:,idomp),nperi,subd,nirep,one,ework,nperi)

    !(E-CB^{-1}D)^{-1}CB^{-1}(f-DE^{-1}g)
    call zgesv(nperi,nblck,ework(:,:),nperi,ipiv,gwork(:,:),nperi,info)

    !(f-DE^{-1}g)+D(E-CB^{-1}D)^{-1}CB^{-1}(f-DE^{-1}g)
    call zgemm("N","N",nirep,nblck,nperi,one,subd,nirep,gwork,nperi,one,fwork,nirep)

    !B^{-1}fwork
    call zgetrs('N',nirep,nblck,subb(:,:,idomp),nirep,ipiv_b(:,idomp),fwork(:,:),nirep,info) !Ay=e
    ymat(1:nirep,:)=fwork(:,:)
    ! -------------------(ここまで)----xを求める-------------------------------------------------
    ! -------------------(ここから)----yを求める-------------------------------------------------
    !g-Cx
    gwork(:,:)=vmat(nirep+1:ne,:)
    call zgemm("N","N",nperi,nblck,nirep,-one,subc(:,:,idomp),nperi,fwork,nirep,one,gwork,nperi)

    !E^{-1}(g-Cx)
    call zgetrs('N',nperi,nblck,sube_lu(:,:),nperi,ipiv_d(:),gwork(:,:),nperi,info)
    ymat(nirep+1:ne,:)=gwork(:,:)
    ! -------------------(ここまで)----yを求める-------------------------------------------------
    deallocate(fwork,gwork,ework,subd,sube,sube_lu,ipiv_d,ipiv)
  end subroutine woodbury

  subroutine hwoodbury(idomp,ibril,nirep,nperi,vmat,ymat)
    use class_hbem
    implicit none
    integer,intent(in)::ibril,nirep,nperi,idomp
    complex(8),allocatable::fwork1(:,:),fwork2(:,:)
    complex(8),allocatable::gwork(:,:)
    complex(8),intent(in)::vmat(:,:)
    complex(8),intent(out)::ymat(:,:)
    type(c_ptr)::hdmat
    type(c_ptr)::hemat,hemat_lu

    allocate(fwork1(nirep,nblck),fwork2(nirep,nblck),gwork(nperi,nblck))

    call build_hmat_ibril(ibril,idomp,hdmat,hemat)
    hemat_lu=clone_hmatrix(hemat)

    call lrdecomp_hmatrix(hemat_lu,c_null_ptr,tol)

    gwork(:,:)=vmat(nirep+1:ne,:)
    !E^{-1}g
    call lusolve_hmatrix(false,hemat_lu,nperi,nblck,gwork(:,:))

    fwork1(:,:)=vmat(1:nirep,:)
    !f-DE^{-1}g
    call addmul_hmat_amat_amat(-one,nirep,nperi,nblck,false,hdmat,false,gwork(:,:),false,fwork1(:,:))

    gwork(:,:)=zero
    !CB^{-1}(f-DE^{-1}g)
    call addmul_hmat_amat_amat(one,nperi,nirep,nblck,false,hcbinvmat(idomp),false,&
         fwork1(:,:),false,gwork(:,:))

    !E-CB^{-1}D
    call addmul_hmatrix(-one,false,hcbinvmat(idomp),false,hdmat,c_null_ptr,tol,&
         hemat)

    call lrdecomp_hmatrix(hemat,c_null_ptr,tol)

    !(E-CB^{-1}D)^{-1}CB^{-1}(f-DE^{-1}g)
    call lusolve_hmatrix(false,hemat,nperi,nblck,gwork(:,:))

    !(f-DE^{-1}g)+D(E-CB^{-1}D)^{-1}CB^{-1}(f-DE^{-1}g)
    call addmul_hmat_amat_amat(one,nirep,nperi,nblck,false,hdmat,false,&
         gwork(:,:),false,fwork1(:,:))

    fwork2(:,:)=zero
    !B^{-1}fwork1
    call addmul_hmat_amat_amat(one,nirep,nirep,nblck,false,hbmat(idomp),false,&
         fwork1(:,:),false,fwork2(:,:))

    ymat(1:nirep,:)=fwork2(:,:)
    ! -------------------(ここまで)----xを求める-------------------------------------------------
    ! -------------------(ここから)----yを求める-------------------------------------------------
    gwork(:,:)=vmat(nirep+1:ne,:)

    !g-Cx
    call addmul_hmat_amat_amat(-one,nperi,nirep,nblck,false,hcmat(idomp),false,&
         fwork2(:,:),false,gwork(:,:))

    !E^{-1}(g-Cx)
    call lusolve_hmatrix(false,hemat_lu,nperi,nblck,gwork(:,:))

    ymat(nirep+1:ne,:)=gwork(:,:)
    ! -------------------(ここまで)----yを求める-------------------------------------------------
    deallocate(fwork1,fwork2,gwork)
    call del_hmatrix(hemat)
    call del_hmatrix(hemat_lu)
    call del_hmatrix(hdmat)

  end subroutine hwoodbury

  subroutine hlu(idomp,ibril,vmat,ymat)
    use class_hbem
    implicit none
    integer,intent(in)::ibril,idomp
    complex(8),intent(in)::vmat(ne,nblck)
    complex(8),intent(out)::ymat(ne,nblck)
    type(c_ptr),target::hmats(4)
    type(c_ptr)::hamat

    hmats(1)=clone_hmatrix(hbmat(idomp))
    hmats(2)=clone_hmatrix(hcmat(idomp))
    call build_hmat_ibril(ibril,idomp,hmats(3),hmats(4))
    hamat = integrate_hmatrix(all_cluster,all_cluster,c_loc(hmats(1)))

    ! call coarsen_hmat(hamat, c_null_ptr, tol)
    call lrdecomp_hmatrix(hamat,c_null_ptr,tol)
    ymat(:,:)=vmat(:,:)
    call lusolve_hmatrix(false,hamat,ne,nblck,ymat(:,:))
    call del_hmatrix(hamat)

  end subroutine hlu

end subroutine solver

!           nirep        nperi     nblck       nblck
!      _________________________   _______     ______
!      |              |         | |       |   |      |
!      |              |         | |       |   |      |
!      |              |         | |       |   |      |
!nirep |   hbmat      |  hdmat  | |  x    |   |  f   |
!      |              |         | |       | = |      |
!      |______________|_________| |_______|   |______|
!      |              |         | |       |   |      |
!nperi |   hcmat      |  hemat  | |  y    |   |  g   |
!      |______________|_________| |_______|   |______|
!                 A(z)              ymat        vmat
