subroutine alloc_kernelsin(rows,cols,id)
  use globals
  ! use omp_lib
  implicit none
  integer :: rows,cols,id

  allocate(krnl(id)%s(rows,cols))
  allocate(krnl(id)%d(rows,cols))
  allocate(krnl(id)%ds1(rows,cols))
  allocate(krnl(id)%ds2(rows,cols))
  allocate(krnl(id)%dd1(rows,cols))
  allocate(krnl(id)%dd2(rows,cols))
  allocate(krnl(id)%flag(rows,cols))

  krnl(id)%flag(:,:)=0
  
end subroutine alloc_kernelsin

subroutine dealloc_kernelsin(id)
  use globals
  ! use omp_lib
  implicit none
  integer :: id 

  deallocate(krnl(id)%s)
  deallocate(krnl(id)%d)
  deallocate(krnl(id)%ds1)
  deallocate(krnl(id)%ds2)
  deallocate(krnl(id)%dd1)
  deallocate(krnl(id)%dd2)
  deallocate(krnl(id)%flag)
  
end subroutine dealloc_kernelsin
