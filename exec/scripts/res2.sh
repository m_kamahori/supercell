DIR=./fig/20170728
step1=0;step2=0
eps=5;tol=12
for n in `seq 10 11`
do
    for rad in 4
    do
	for islv in 0 1 2 3
	do
	    echo "n=${n}, rad=${rad}, islv=${islv}"
	    if [ $islv -eq 0 ];then
		out=log.dat_wood_rad0.${rad}
	    elif [ $islv -eq 1 ];then
		out=log.dat_hbmat_rad0.${rad}
	    elif [ $islv -eq 2 ];then
		out=log.dat_hwood_rad0.${rad}
	    elif [ $islv -eq 3 ];then
		out=log.dat_hamat_rad0.${rad}
	    fi
	    touch ${out}
	    rm log.dat
	    xe=`echo 2^$n |bc`
	    if [ $step1 -eq 0 ];then
	    	rm band/*.bnd configuration/*.cnf lsf/*.ls2 naiten/*.ip2 x.dat root.res
	    fi
	    cd ../src && make && cd ../exec
	    rm td/* #fort*
	    echo 24               !threadの数 > input
	    echo $xe              !x yousosu >> input # 偶数のみ
	    echo $xe              !y yousosu >> input
	    echo 1.d0             !a_1 no okisa >> input
	    echo 1.d0             !a_2 no okisa >> input
	    echo 6.d1             !a_1 to a_2 no nasu kaku >> input
	    echo 1                !0:assymmetry, 1:1/4 symmetry, 2:1/8 symmetry >> input
	    echo 0                !0:gamma点なし, 1:gamma点あり >> input
	    echo ${rad}.d-1            !ana no hankei >> input
	    echo ${islv}	  !0:woodbury, 1:hbmat only, 2:hmat_woodbury, 3:hamat >> input
	    echo 100	          !ev no saidai kosu >> input
	    echo 128              !nskbn:積分点の数 >> input
	    echo 1                !sekibun en no kazu >> input
	    echo 3.6d0 0.d0       !1st sekibun en no chusin >> input
	    echo 4.d0             !rad:積分円の半径 >> input
	    echo 0.5d0            !margin:積分円のマージン >> input
	    echo 5                !nblck:blockの数、固有値の重複度がnblck以下になるようにしておく >> input
	    echo 10               !nhank:Hankel行列のサイズ >> input
	    echo $step1           !start step >> input
	    echo $step2           !end step >> input
	    echo 2                !1youso no bunkatsusu >> input
	    echo 2                !chiisaku shitai mode >> input
	    echo 3                !okiku shitai mode >> input
	    echo 0.d0             !offset1 >> input
	    echo 0.08d0           !offset2 >> input
	    echo 5.d-2            !拡散方程式時間差分の幅 >> input
	    echo 4.d-2            !拡散方程式時間差分の幅 >> input
	    echo 5                !tloop1:どのステップまでdeltat1で計算するか >> input
	    echo 6                !tloop2:どのステップからdeltat2で計算するか >> input
	    echo 1.d0             !prm_c:1d0で固定 >> input
	    # echo 5.d-4            !複雑度係数 >> input
	    echo 8.d-7            !複雑度係数 >> input
	    echo 32                !nmin >> input
	    echo 32                !nminin >> input
	    echo 1.0              !eta: admissible conditionのeta >> input
	    echo 1.0              !eta_in: 内点のeta >> input
	    echo 1.d-${eps}            !eps_aca: ACAの許容誤差 >> input
	    echo 1.d-${tol}            !tol: H行列計算の許容誤差 >> input
	    echo 0                !cflag: agglomerationするかしないか >> input
	    echo 1.d-4            !ceps: agglomerationの許容誤差 >> input
	    ./a.out < input
	    cat log.dat >> ${out}
	done
    done
done
cp log.dat_* $DIR
