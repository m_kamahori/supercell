program shikaku
  implicit none
  !---------inputs-----------
  integer::ne!要素数/4
  real(8)::xmin  !左下のx座標
  real(8)::ymin  !左下のy座標
  real(8)::length!一辺の長さ
  !--------------------------
  integer::i
  real(8)::x,y
  
  read(*,*) ne,xmin,ymin,length

  open(1,file="ls2d0000.el2")
  write(1,*) 4*ne
  x=xmin
  y=ymin
  do i=1,ne !底辺
     x=x+length/dble(ne)
     write(1,*) i,x,y
     write(2,*) x,y
  end do
  do i=1,ne !右辺
     y=y+length/dble(ne)
     write(1,*) i+ne,x,y
     write(2,*) x,y
  end do
  do i=1,ne !上辺
     x=x-length/dble(ne)
     write(1,*) i+2*ne,x,y
     write(2,*) x,y
  end do
  do i=1,ne !左辺
     y=y-length/dble(ne)
     write(1,*) i+3*ne,x,y
     write(2,*) x,y
  end do
  write(1,*) 4*ne
  do i=1,4*ne-1
     write(1,*) i,i,i+1
  end do
  write(1,*) i,4*ne,1
  close(1)

end program shikaku
