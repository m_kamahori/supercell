program circle
  implicit none
  !---------inputs-----------
  integer::ne!要素数
  integer::n !周期境界の一辺の分割数
  real(8)::cx!中心のx座標
  real(8)::cy!中心のy座標
  real(8)::r !半径
  !--------------------------
  integer::i
  real(8)::x,y,theta,pi

  integer::nn(4)

  pi=acos(-1.d0)

  read(*,*) ne,n,cx,cy,r

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  nn(:)=n
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  open(1,file="ls2d0000.el2")
  write(1,*) ne+sum(nn)
  do i=1,ne
     theta=2.d0*pi*(i-1)/dble(ne)
     x=cx+r*cos(theta)
     y=cy+r*sin(theta)
     write(1,*) i,x,y
  end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!periodic boundary
  x=-0.5d0
  y=-0.5d0
  do i=1,nn(1)
     x=x+1.d0/dble(nn(1))
     write(1,*) i,x+0.5d0,y+0.5d0
  end do

  do i=1,nn(2)
     y=y+1.d0/dble(nn(2))
     write(1,*) i+nn(1),x+0.5d0,y+0.5d0
  end do

  do i=1,nn(3)
     x=x-1.d0/dble(nn(3))
     write(1,*) i+sum(nn(1:2)),x+0.5d0,y+0.5d0
  end do

  do i=1,nn(4)
     y=y-1.d0/dble(nn(4))
     write(1,*) i+sum(nn(1:3)),x+0.5d0,y+0.5d0
  end do
  write(1,*) ne+sum(nn)
!!!!!!!!!!!!!!!!!!!!!!!! circle
  do i=1,ne-1
     write(1,*) i,i,i+1,1,0,0
  end do
  write(1,*) ne,ne,1,1,0,0
!!!!!!!!!!!!!!!!!!!!!!!! shita 
  do i=ne+1,ne+nn(1)-1
     write(1,*) i,i,i+1,10,-2,ne+sum(nn(1:3))-(i-ne)
  end do
  write(1,*) ne+nn(1),sum(nn),1,10,-2,nn(1)+nn(2)+nn(3)
!!!!!!!!!!!!!!!!!!!!!!!! migi
  i=ne+nn(1)
  write(1,*) i+1,i,i+1,10,1,sum(nn)
  do i=nn(1)+1,sum(nn(1:2))-1
     write(1,*) i+1,i,i+1,10,1,sum(nn)-(i-nn(1))
  end do
!!!!!!!!!!!!!!!!!!!!!!!! ue
  i=sum(nn(1:2))
  write(1,*) i+1,i,i+1,10,2,nn(1)
  do i=sum(nn(1:2))+1,sum(nn(1:3))-2
     write(1,*) i+1,i,i+1,10,2,nn(1)-(i-sum(nn(1:2)))
  end do
  i=sum(nn(1:3))-1
  write(1,*) i+1,i,i+1,10,2,1
!!!!!!!!!!!!!!!!!!!!!!!! hidari
  i=sum(nn(1:3))
  write(1,*) i+1,i,i+1,10,-1,sum(nn(1:2))
  do i=sum(nn(1:3))+1,sum(nn(1:4))-1
     write(1,*) i+1,i,i+1,10,-1,sum(nn(1:2))-(i-sum(nn(1:3)))
  end do

  close(1)
  
end program circle
