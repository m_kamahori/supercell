program srr
  implicit none
!!$  !---------inputs-----------
!!$  integer::ne!要素数/4
!!$  real(8)::xmin  !左下のx座標
!!$  real(8)::ymin  !左下のy座標
!!$  real(8)::length!一辺の長さ
!!$  !--------------------------
  integer::i,itmp
  integer::n(4),m(4),nn(4),nq
  real(8)::x,y,rad

  real(8),parameter::pi=acos(-1.d0)

  real(8),parameter::pich=0.1d0
  real(8),parameter::h1=0.0129534d0
  real(8),parameter::h2=0.0103752d0
  real(8)::theta,thetamin,thetamax

  read(*,*) itmp

  m(:)=itmp
  ! m(:)=5
  n(1)=m(1)*2*3.14
  n(2)=n(1)*2
  n(3)=n(1)*3
  n(4)=n(1)*4
  nn(:)=m(1)*10

  open(101,file="step0000.el2")
  write(101,*) sum(n)+sum(m)+sum(nn)

!!$  thetamax= pi-asin(h2*0.5d0/pich)
!!$  thetamin=-pi+asin(h2*0.5d0/pich)
  thetamax=pi-abs(atan(h2*0.5d0/pich))
  thetamin=-pi+abs(atan(h2*0.5d0/pich))
  rad=pich
  do i=1,n(1)
     theta=(thetamax-thetamin)/dble(n(1)-1)*dble(i)+thetamin-(thetamax-thetamin)/dble(n(1)-1)
     x=rad*cos(theta)
     y=rad*sin(theta)
     write(101,*) i,x+0.5d0,y+0.5d0
  end do

  do i=1,m(1)
     x=x-pich/dble(m(1)+1)
     write(101,*) i+n(1),x+0.5d0,y+0.5d0
  end do



!!$  thetamax=pi-abs(atan(y/x))
!!$  thetamin=-pi+abs(atan(y/x))
  thetamin=-pi+abs(atan(h2*0.5d0/(2d0*pich)))
  thetamax= pi-abs(atan(h2*0.5d0/(2d0*pich)))
  rad=2.d0*rad
  do i=1,n(2)
     theta=(thetamin-thetamax)/dble(n(2)-1)*dble(i)+thetamax-(thetamin-thetamax)/dble(n(2)-1)
     x=rad*cos(theta)
     y=rad*sin(theta)
     write(101,*) i+n(1)+m(1),x+0.5d0,y+0.5d0
  end do

  do i=1,m(2)
     x=x+pich/dble(m(2)+1)
     write(101,*) i+n(1)+m(1)+n(2),x+0.5d0,y+0.5d0
  end do

  thetamin=        abs(atan(h1*0.5d0/(3d0*pich)))
  thetamax=2.d0*pi-abs(atan(h1*0.5d0/(3d0*pich)))
  rad=1.5d0*rad
  do i=1,n(3)
     theta=(thetamax-thetamin)/dble(n(3)-1)*dble(i)+thetamin-(thetamax-thetamin)/dble(n(3)-1)
     x=rad*cos(theta)
     y=rad*sin(theta)
     write(101,*) i+n(1)+m(1)+n(2)+m(2),x+0.5d0,y+0.5d0
  end do

  do i=1,m(3)
     x=x+pich/dble(m(3)+1)
     write(101,*) i+n(1)+m(1)+n(2)+m(2)+n(3),x+0.5d0,y+0.5d0
  end do


  thetamin=        abs(atan(h1*0.5d0/(4d0*pich)))
  thetamax=2.d0*pi-abs(atan(h1*0.5d0/(4d0*pich)))
  rad=4.d0/3.d0*rad
  do i=1,n(4)
     theta=(thetamin-thetamax)/dble(n(4)-1)*dble(i)+thetamax-(thetamin-thetamax)/dble(n(4)-1)
     x=rad*cos(theta)
     y=rad*sin(theta)
     write(101,*) i+n(1)+m(1)+n(2)+m(2)+n(3)+m(3),x+0.5d0,y+0.5d0
  end do

  do i=1,m(4)
     x=x-pich/dble(m(4)+1)
     write(101,*) i+n(1)+m(1)+n(2)+m(2)+n(3)+m(3)+n(4),x+0.5d0,y+0.5d0
  end do

  x=-0.5d0
  y=-0.5d0
  ! shita
  do i=1,nn(1)
     x=x+1.d0/dble(nn(1))
     !            節点番号       ,x座標  ,y座標
     write(101,*) i+sum(n)+sum(m),x+0.5d0,y+0.5d0
  end do
  ! migi
  do i=1,nn(2)
     y=y+1.d0/dble(nn(2))
     write(101,*) i+sum(n)+sum(m)+nn(1),x+0.5d0,y+0.5d0
  end do
  ! ue
  do i=1,nn(3)
     x=x-1.d0/dble(nn(3))
     write(101,*) i+sum(n)+sum(m)+sum(nn(1:2)),x+0.5d0,y+0.5d0
  end do
  ! hidari
  do i=1,nn(4)
     y=y-1.d0/dble(nn(4))
     write(101,*) i+sum(n)+sum(m)+sum(nn(1:3)),x+0.5d0,y+0.5d0
  end do
  

  !!!!!!!!

  write(101,*) sum(n)+sum(m)+sum(nn)
  do i=1,sum(n(1:2))+sum(m(1:2))-1
     write(101,*) i,i,i+1,1,0,0
  end do
  write(101,*) sum(n(1:2))+sum(m(1:2)),sum(n(1:2))+sum(m(1:2)),1,1,0,0

  do i=sum(n(1:2))+sum(m(1:2))+1,sum(n)+sum(m)-1
     write(101,*) i,i,i+1,1,0,0
  end do
  write(101,*) sum(n)+sum(m),sum(n)+sum(m),1+sum(n(1:2))+sum(m(1:2)),1,0,0
!!!!!!

  nq=sum(n)+sum(m)
  !               要素番号         ,node番号              ,node番号              ,ibc,iperi(:,:)
  write(101,*)    nq+1             ,nq+sum(nn)  ,nq+1            ,10 ,-2,nq+sum(nn(1:2))+1
!shita
  do i=2,nn(1) 
     write(101,*) i+nq             ,nq+i-1        ,nq+i            ,10 ,-2,nq+sum(nn(1:2))+i
  end do
!hidari
  do i=1,nn(4)
     write(101,*) i+nq+nn(1)       ,nq+sum(nn)-i,nq+sum(nn)-i+1,10 ,-1,nq+sum(nn(1:3))+i
  end do
!ue
  do i=1,nn(3)
     write(101,*) i+nq+sum(nn(1:2)),nq+sum(nn(1:3))-i  ,nq+sum(nn(1:3))-i+1  ,10 ,2 ,nq+i
  end do
!migi
  do i=1,nn(2)
     write(101,*) i+nq+sum(nn(1:3)),nq+nn(1)+i-1     ,nq+nn(1)+i         ,10 ,1 ,nq+nn(1)+i
  end do

  close(101)
end program srr
