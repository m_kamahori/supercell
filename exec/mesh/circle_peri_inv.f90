program circle_peri_inv
  implicit none
  !---------inputs-----------
  integer::nq!要素数
  integer,parameter::nqq=10
  real(8)::cx!中心のx座標
  real(8)::cy!中心のy座標
  real(8),parameter::r=0.1d0 !半径
  !--------------------------
  integer::i,itmp
  real(8)::x,y,theta,pi,elml,elmlp
  real(8)::lp !周期境界の一辺の長さ

  integer::nn(4)


  pi=acos(-1.d0)

  ! read(*,*) elml,r

  theta=pi/2/nqq
  elml=r*sin(theta/2.d0)*2.d0
  ! theta=2.d0*asin(elml/(2.d0*r))
  ! nq=2.d0*pi/theta
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! nn(:)=nqq*4/pi
  lp=1.d0-2.d0*r
  nn(:)=lp/elml
  elmlp=lp/nn(1)                !周期境界の長さが全て同じと仮定。
  nq=4*nqq
  ! nn(:)=int(r/elml)*2
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  write(*,*) "edge size:",elml
  write(*,*) nq,nn(:)

  open(1,file="step0000.el2")
  write(1,*) nq+sum(nn)
  cx=0.d0;cy=1.d0
  do i=1,nqq
     x=cx+r*cos(-i*theta)
     y=cy+r*sin(-i*theta)
     write(1,*) i,x,y
  end do
  cx=1.d0;cy=1.d0
  do i=nqq+1,nqq*2
     x=cx+r*cos(-i*theta)
     y=cy+r*sin(-i*theta)
     write(1,*) i,x,y
  end do
  cx=1.d0;cy=0.d0
  do i=nqq*2+1,nqq*3
     x=cx+r*cos(-i*theta)
     y=cy+r*sin(-i*theta)
     write(1,*) i,x,y
  end do
  cx=0.d0;cy=0.d0
  do i=nqq*3+1,nq
     x=cx+r*cos(-i*theta)
     y=cy+r*sin(-i*theta)
     write(1,*) i,x,y
  end do


!!!!!!!!!!!!!!!!!!!!!!!!periodic boundary
  x=r;y=0.d0
  ! shita
  do i=1,nn(1)
     x=x+elmlp
     write(1,*) nq+i,x,y
  end do
  ! migi
  x=1.d0;y=r
  do i=1,nn(2)
     y=y+elmlp
     write(1,*) nq+nn(1)+i,x,y
  end do
  ! ue
  x=1.d0-r;y=1.d0
  do i=1,nn(3)
     x=x-elmlp
     write(1,*) nq+sum(nn(1:2))+i,x,y
  end do
  ! hidari
  x=0.d0;y=1.d0-r
  do i=1,nn(4)
     y=y-elmlp
     write(1,*) nq+sum(nn(1:3))+i,x,y
  end do

!!!!!!!!!!node!!!!!!!!!!
  write(1,*) nq+sum(nn)
!!!!!!!!!!!!!!!!!!!!!!!! circle
  write(1,*) 1,nq+sum(nn(1:3)),1,1,0,0
  do i=2,nqq
     write(1,*) i,i-1,i,1,0,0
  end do
  write(1,*) nqq+1,nq+sum(nn(1:2)),nqq+1,1,0,0
  do i=nqq+2,nqq*2
     write(1,*) i,i-1,i,1,0,0
  end do
  write(1,*) nqq*2+1,nq+nn(1),nqq*2+1,1,0,0
  do i=nqq*2+2,nqq*3
     write(1,*) i,i-1,i,1,0,0
  end do
  write(1,*) nqq*3+1,nq+sum(nn),nqq*3+1,1,0,0
  do i=nqq*3+2,nq
     write(1,*) i,i-1,i,1,0,0
  end do

  ! peri
  !               要素番号             ,node番号             ,node番号              ,ibc,iperi(:,:)
  ! write(1,*)    nq+1             ,nq+sum(nn)       ,nq+1               ,10 ,-2,nq+sum(nn(1:2))+1
!shita
  do i=1,nn(1)
     write(1,*) i+nq             ,nq+i-1           ,nq+i               ,10 ,-2,nq+sum(nn(1:2))+i
  end do
!hidari
  do i=1,nn(4)-1
     write(1,*) i+nq+nn(1)       ,nq+sum(nn)-i     ,nq+sum(nn)-i+1     ,10 ,-1,nq+sum(nn(1:3))+i
  end do
  write(1,*)    nq+nn(1)+nn(4)   ,nqq              ,nq+sum(nn)-nn(4)+1 ,10 ,-1,nq+sum(nn)
!ue
  do i=1,nn(3)-1
     write(1,*) i+nq+sum(nn(1:2)),nq+sum(nn(1:3))-i,nq+sum(nn(1:3))-i+1,10 ,2 ,nq+i
  end do
  write(1,*)    nq+sum(nn(1:3))  ,nqq*2            ,nq+sum(nn(1:2))+1  ,10 ,2 ,nq+nn(1)
!migi
  write(1,*)    nq+sum(nn(1:3))+1,nqq*3            ,nq+nn(1)+1         ,10 ,1 ,nq+nn(1)+1
  do i=2,nn(2)
     write(1,*) i+nq+sum(nn(1:3)),nq+nn(1)+i-1     ,nq+nn(1)+i         ,10 ,1 ,nq+nn(1)+i
  end do

! !!!!!!!!!!!!!!!!!!!!!!!! shita 
!   do i=nq+1,nq+nn(1)
!      write(1,*) i,i,i+1,10,-2,nq+sum(nn(1:3))-(i-nq-1)
!   end do
!   ! write(1,*) nq+nn(1),sum(nn),1,10,-2,nn(1)+nn(2)+nn(3)
! !!!!!!!!!!!!!!!!!!!!!!!! migi
!   ! i=nq+nn(1)
!   ! write(1,*) i+1,i,i+1,10,1,sum(nn)
!   do i=nq+nn(1)+1,nq+sum(nn(1:2))
!      write(1,*) i,i,i+1,10,1,nq+sum(nn)-(i-nq-nn(1)-1)
!   end do
! !!!!!!!!!!!!!!!!!!!!!!!! ue
!   ! i=sum(nn(1:2))
!   ! write(1,*) i+1,i,i+1,10,2,nn(1)
!   do i=nq+sum(nn(1:2))+1,nq+sum(nn(1:3))
!      write(1,*) i,i,i+1,10,2,nq+nn(1)-(i-nq-sum(nn(1:2))-1)
!   end do
!   ! i=sum(nn(1:3))-1
!   ! write(1,*) i+1,i,i+1,10,2,1
! !!!!!!!!!!!!!!!!!!!!!!!! hidari
!   ! i=sum(nn(1:3))
!   ! write(1,*) i+1,i,i+1,10,-1,sum(nn(1:2))
!   do i=nq+sum(nn(1:3))+1,nq+sum(nn)-1
!      write(1,*) i,i,i+1,10,-1,nq+sum(nn(1:2))-(i-nq-sum(nn(1:3))-1)
!   end do
!   write(1,*) nq+sum(nn),nq+sum(nn),nq+1,10,-1,nq+nn(1)+1
! !!!!!!!!!!node!!!!!!!!!!

  close(1)

  
  
end program circle_peri_inv
