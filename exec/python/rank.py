import matplotlib.pyplot as plt
import numpy as np


inpfile='rank.res'
fin = open(inpfile)
l = sum(1 for line in fin)
l = int(np.sqrt(l))

u =np.loadtxt(inpfile,delimiter=",")
xtmp = np.arange(l+1)
ytmp = np.arange(l+1)
cx, cy = np.meshgrid(xtmp, ytmp)
cz_pre=np.zeros((l+1,l+1))
cz_pos=np.zeros((l+1,l+1))
for ix in range(l):
    for iy in range(l):
        i=u[l*iy+ix][0]
        j=u[l*iy+ix][1]
        cz_pos[i][j]=u[l*iy+ix][2]
        cz_pre[i][j]=u[l*iy+ix][3]

maxrank=np.max(cz_pre)
if (maxrank % 2) == 1:
    maxrank = maxrank + 1

fig1, ax1 = plt.subplots(figsize=(9.0,7.5))
plt.autumn()
# plt.bone()
im1=ax1.pcolor(cx,cy,cz_pre,vmin=1,vmax=maxrank)
ax1.axis([0,l,l,0])
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
cbar1=fig1.colorbar(im1,ticks=[1,int(maxrank/2),maxrank])
cbar1.ax.tick_params(labelsize=25)

fig2, ax2 = plt.subplots(figsize=(9.0,7.5))
plt.autumn()
# plt.bone()
im2=ax2.pcolor(cx,cy,cz_pos,vmin=1,vmax=maxrank)
ax2.axis([0,l,l,0])
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
cbar2=fig2.colorbar(im2,ticks=[1,int(maxrank/2),maxrank])
cbar2.ax.tick_params(labelsize=25)

fin = open("block.dat")
x=[]
y=[]

for line in fin:
    dat = line.rstrip('\n')
    dat = dat.split(',')
    if int(dat[0]) == -1:
        ax1.plot(x,y,color='black',linewidth=0.3)
        ax2.plot(x,y,color='black',linewidth=0.3)
        x=[]
        y=[]
    else:
        x.append(int(dat[0]))
        y.append(int(dat[1]))
fin.close()

plt.show()
outfile1='./pre.png'
outfile2='./pos.png'
fig1.savefig(outfile1) 
fig2.savefig(outfile2) 

