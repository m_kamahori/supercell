#cd ../src && cp makefile_prop makefile && make clean && make && cd ../exec
HOME=`pwd`
DIR=/home/m_kamahori/git-repos/res1/to_3.3_ver2
if [ ! -e ${DIR} ];then
    mkdir -p ${DIR}
    echo "Please put lsf file in ${DIR}"
else
    mkdir ${DIR}/lsf ${DIR}/band ${DIR}/bp ${DIR}/ip2 ${DIR}/ip ${DIR}/evslct ${DIR}/td ${DIR}/mesh ${DIR}/ref ${DIR}/sd # 必要なディレクトリを作成
    cp go.sh ${DIR}/
    cd ../src &&  make && cd ../exec
    step1=0;step2=999;mflag=1
    xe=32;eps=8;tol=10;islv=-1
    if [ $step1 -eq 0 ];then
	cd $DIR && rm band/*.bnd bp/*.bp lsf/*.ls2 ip2/*.ip2 ip/*.ip;cd $HOME
	if [ $mflag -eq 0 ];then
	    mesher=srr.f90
	    cd mesh && gfortran $mesher && echo 3 | ./a.out&& cd -
	    # mesher=square.f90
	    # cd mesh && gfortran $mesher && echo 1280 40  | ./a.out&& cd -
	fi
    fi
    echo 32               !threadの数 > input
    echo ${mflag}         !1:generate meshfile from lsf, 0:use prepared mesh file >> input
    echo $xe              !x yousosu >> input # 偶数のみ
    echo $xe              !y yousosu >> input
    echo 1.d0              !固定設計領域のサイズx >> input
    echo 1.d0              !固定設計領域のサイズy >> input
    echo 3              !supercellのサイズ >> input
    echo 9.d1             !a_1 to a_2 no nasu kaku >> input
    echo 2                !0:assymmetry, 1:1/4 symmetry, 2:1/8 symmetry >> input
    echo 0                !0:gamma点なし, 1:gamma点あり >> input
    echo ${islv}	      !-1:amat, 0:woodbury, 1:hbmat only, 2:hmat_woodbury, 3:hamat >> input
    echo 100	      !ev no saidai kosu >> input
    echo 32               !nskbn:積分点の数 >> input
    echo 2.d0            !minomg:bottom angular frequency >> input
    echo 4.5d0            !maxomg:top angular frequency >> input
    # echo 2.5d0            !minomg:bottom angular frequency >> input
    # echo 4.d0            !maxomg:top angular frequency >> input
    echo 6                !nir:sekibun en no kazu >> input
    echo 0.8d0             !init_rad:積分円の半径 >> input
    echo 0.4d0           !sekibun en no hankei no saidaichi >> input
    # echo 0.8d0           !sekibun en no hankei no saidaichi >> input
    echo 0.1d0            !mrate:積分円の半径に対するマージンの割合 >> input
    echo 5                !nblck:blockの数、固有値の重複度がnblck以下になるようにしておく >> input
    echo 10               !nhank:Hankel行列のサイズ >> input
    echo $step1           !start step >> input
    echo $step2           !end step >> input
    echo 1                !1youso no bunkatsusu >> input
    echo -1               !opt_flag:1:maximisation, -1:minimisation >> input
    echo 3.3d0            !target local resonant angular frequency >> input
    echo 0.d0             !offset1 >> input
    echo 0.08d0           !offset2 >> input
    # echo 5.d-1            !拡散方程式時間差分の幅 >> input
    # echo 5.d-2            !拡散方程式時間差分の幅 >> input
    echo 4.d-2            !拡散方程式時間差分の幅 >> input
    echo 2.d-2            !拡散方程式時間差分の幅 >> input
    echo 42               !tloop1:どのステップまでdeltat1で計算するか >> input
    echo 43               !tloop2:どのステップからdeltat2で計算するか >> input
    echo 1.d0             !prm_c:1d0で固定 >> input
    # echo 5.d-3            !複雑度係数 >> input
    # echo 5.d-4            !複雑度係数 >> input
    echo 5.d-5            !複雑度係数 >> input
    # echo 5.d-6            !複雑度係数 >> input
    # echo 8.d-7            !複雑度係数 >> input
    echo 32               !nmin >> input
    echo 32               !nminin >> input
    echo 1.d0             !eta: admissible conditionのeta >> input
    echo 1.d0             !eta_in: 内点のeta >> input
    echo 1.d-${eps}       !eps_aca: ACAの許容誤差 >> input
    echo 1.d-${tol}       !tol: H行列計算の許容誤差 >> input
    echo 0                !cflag: agglomerationするかしないか >> input
    echo 1.d-4            !ceps: agglomerationの許容誤差 >> input
    echo '"'${DIR}'"'     !Input/Output directory >>input
    echo '"'${DIR}/step0060.ls2'"'     !lsf of external unitcells >>input
    ./a.out < input
fi
